#!/usr/bin/env python
# encoding: utf-8

import argparse
import urllib2
import re

import roslib
roslib.load_manifest('konz_datasets')

from download import download
from dataset import Dataset
from bag import Bag
from replace_camera_info_messages import replace_camera_info_messages

url = 'http://vision.in.tum.de/rgbd/dataset/'


def get_sequences(validation=False, calibration=False, dataset=None):
    """
    Fetch the list of sequences from the server.

    Parameters
    ----------
    validation : boolean
        Whether to include validation sequences in the list.
    calibration : boolean
        Whether to include calibration sequences in the list.
    dataset : int
        If provided then output only the sequences of this dataset.
    """
    html = urllib2.urlopen(url).read()
    if dataset in [1, 2, 3]:
        regex = r"href='/rgbd/dataset/freiburg%i/(\w+)\.bag'" % dataset
    else:
        regex = r"href='/rgbd/dataset/freiburg\d/(\w+)\.bag'"
    bags = re.findall(regex, html)
    if not validation:
        bags = [bag for bag in bags if not re.search(r'validation', bag)]
    if not calibration:
        bags = [bag for bag in bags if not re.search(r'calibration', bag)]
    return bags


def download_sequence(name):
    m = re.search(r'(freiburg(\d))', name).groups()
    ds = Dataset('fr' + m[1])
    u = url + m[0] + '/' + name
    download(u + '.bag', ds.folders.get_path())
    download(u + '-groundtruth.txt', ds.folders.groundtruth)
    return ds.folders('%s.bag' % name)


if __name__ == '__main__':
    parser = argparse.ArgumentParser(description='''
    Download 'RGB-D SLAM Dataset' from the CVPR group of TU Munich,
    see: http://vision.in.tum.de/data/datasets/rgbd-dataset

    The dataset is distributed as multiple bag files with different sequences.
    This script can list all available sequences and download any of them
    together with the corresponding ground truth trajectory file.
    ''', formatter_class=argparse.RawDescriptionHelpFormatter)
    parser.add_argument('names', nargs='*',  help='sequence name(s)')
    parser.add_argument('--all', action='store_true',
                        help='download all available sequences that are not '
                        'present on the local machine')
    parser.add_argument('--list', action='store_true',
                        help='list all available sequences')
    parser.add_argument('--include-validation', action='store_true',
                        help='include validation sequences in the list')
    parser.add_argument('--include-calibration', action='store_true',
                        help='include calibration sequences in the list')
    parser.add_argument('--replace-camera-info', action='store_true',
                        help='replace camera info messages in the downloaded '
                        'bag file(s) with the dataset calibration')
    args = parser.parse_args()

    if args.list:
        for s in sorted(get_sequences(args.include_validation,
                                      args.include_calibration)):
            print s
    elif args.all or args.names:
        if args.all:
            args.names = list()
            for d in [1, 2, 3]:
                ds = Dataset('fr%i' % d)
                for s in sorted(get_sequences(args.include_validation,
                                              args.include_calibration, d)):
                    if s not in ds.get_bag_names():
                        args.names.append(s)
        for name in args.names:
            bagfile = download_sequence(name)
            if args.replace_camera_info:
                bag = Bag(bagfile)
                replace_camera_info_messages(bag, **bag.dataset.calibration)
    else:
        parser.print_help()
