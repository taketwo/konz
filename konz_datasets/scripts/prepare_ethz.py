#!/usr/bin/env python
# encoding: utf-8

import argparse
import subprocess

import roslib
roslib.load_manifest('konz_datasets')

from dataset import Dataset
from decompose_pointcloud_messages import decompose_pointcloud_messages
from create_intervals_file import create_intervals_file


if __name__ == '__main__':
    parser = argparse.ArgumentParser(description='''
    Prepare all of the bag files in ETHZ dataset for evaluation. That includes
    decomposing point cloud into color/depth/camera info messages, extracting
    ground truth trajectories, and producing intervals files.
    ''', formatter_class=argparse.RawDescriptionHelpFormatter)
    args = parser.parse_args()

    # shell command to extract ground truth trajectory
    cmd = ('rosrun konz_tools extract_trajectory.py -t /vicon_vehicle_20 '
           '-w /ned --transform 0.0182521 -0.00901447 -0.0427566 0.508233 '
           '0.500217 0.510305 0.480699 %s %s')

    ds = Dataset('ethz')
    for bag in ds.get_bags():
        decompose_pointcloud_messages(bag, '/camera/depth/points2')
        subprocess.call((cmd % (bag.abspath, bag.groundtruth)).split())
        create_intervals_file(bag, 0.3, 0.15)
