#!/usr/bin/env python
# encoding: utf-8

import argparse

import roslib
roslib.load_manifest('konz_datasets')

from download import download
from dataset import Dataset

url = 'http://www.files.ethz.ch/asl/ethzasl_kinect_dataset.tar.bz2'


if __name__ == '__main__':
    parser = argparse.ArgumentParser(description='''
    Download 'IROS 2011 Paper Kinect Dataset' from the ASL group of ETH Zurich,
    see: http://projects.asl.ethz.ch/datasets/doku.php?id=kinect:iros2011kinect

    The dataset is distributed in a single archive file. This script will only
    download the archive to the dataset folder.
    ''', formatter_class=argparse.RawDescriptionHelpFormatter)
    args = parser.parse_args()

    ds = Dataset('ethz')
    download(url, ds.folders.get_path())
