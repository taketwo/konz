#!/usr/bin/env python
# encoding: utf-8

import argparse
import subprocess

import roslib
roslib.load_manifest('konz_datasets')

from dataset import Dataset


if __name__ == '__main__':
    parser = argparse.ArgumentParser(description='''
    Prepare all of the bag files in BRSU dataset for evaluation by extracting
    ground truth trajectories of *robot's* frame relative to the map.
    ''', formatter_class=argparse.RawDescriptionHelpFormatter)
    args = parser.parse_args()

    cmd = ('rosrun konz_tools extract_trajectory.py -n -t %s -w %s %s %s')

    ds = Dataset('brsu')
    for bag in ds.get_bags():
        subprocess.call((cmd % (ds.frames[1], ds.frames[2],
                                bag.abspath, bag.groundtruth)).split())
