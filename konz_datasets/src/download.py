# encoding: utf-8

import urllib2
import os


def download(url, path=''):
    from progress_bar import ProgressBar
    # http://stackoverflow.com/a/22776
    filename = os.path.join(path, url.split('/')[-1])
    u = urllib2.urlopen(url)
    f = open(filename, 'wb')
    meta = u.info()
    filesize = int(meta.getheaders("Content-Length")[0])
    print "Filename: %s\nSize:     %.1f Mb" % (filename, filesize / pow(2, 20))
    downloaded = 0
    block = 8192
    pb = ProgressBar(filesize, 30)
    while True:
        buffer = u.read(block)
        if not buffer:
            break
        downloaded += len(buffer)
        f.write(buffer)
        pb.update(downloaded)
    f.close()
    pb.done()
