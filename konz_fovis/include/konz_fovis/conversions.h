#ifndef __KONZ_FOVIS_CONVERSIONS_H
#define __KONZ_FOVIS_CONVERSIONS_H

#include <opencv/cv.h>
#include <sensor_msgs/CameraInfo.h>
#include <sensor_msgs/Image.h>
#include <sensor_msgs/PointCloud2.h>
#include <fovis/fovis.hpp>

namespace konz
{

void colorImageMsgToCvMat(const sensor_msgs::ImageConstPtr& image_msg, cv::Mat& mat);

void depthImageMsgToDepthData(const sensor_msgs::ImageConstPtr& image_msg, float* depth_data);

void pointCloudMsgToCvMat(const sensor_msgs::PointCloud2ConstPtr& cloud_msg,
                          const fovis::CameraIntrinsicsParameters& params,
                          cv::Mat& mat);

void pointCloudMsgToDepthData(const sensor_msgs::PointCloud2ConstPtr& cloud_msg,
                              const fovis::CameraIntrinsicsParameters& params,
                              float* depth_data);

void cameraInfoMsgToCameraIntrinsicsParameters(const sensor_msgs::CameraInfoConstPtr& camera_info_msg,
                                               fovis::CameraIntrinsicsParameters& params);

}

#endif

