#ifndef __KONZ_FOVIS_FOVIS_TRANSFORM_COMPUTER_H
#define __KONZ_FOVIS_FOVIS_TRANSFORM_COMPUTER_H

#include <fovis/frame.hpp>
#include <fovis/depth_image.hpp>
#include <fovis/options.hpp>
#include <fovis/motion_estimation.hpp>
#include <fovis/rectification.hpp>
#include <konz_common/transform_computer.h>

namespace konz
{

/** Computes the transform that relates two frames using fovis library.
  *
  * Fovis library only exposes the VisualOdometry class, which does more than
  * frame-to-frame motion estimation, e.g. it uses the keyframing technique.
  * This class is a simplified version of fovis::VisualOdometry. */
class FovisTransformComputer : public common::TransformComputer
{

public:

  FovisTransformComputer(const std::string& config_filename, const sensor_msgs::CameraInfo::ConstPtr& camera_info);

  virtual ~FovisTransformComputer();

  virtual bool compute(common::Frame::Ptr reference, common::Frame::Ptr target, common::Transform& transform);

  virtual bool compute(common::Frame::Ptr target, common::Transform& transform);

  virtual void setReferenceFrame(common::Frame::Ptr reference);

private:

  // Fovis-related members
  fovis::CameraIntrinsicsParameters rgb_params_;
  fovis::Rectification* rectification_;
  fovis::MotionEstimator* estimator_;

  // Data storage
  fovis::DepthImage* depth_image_;
  float* depth_data_;

  Eigen::Quaterniond estimateInitialRotation(const fovis::OdometryFrame* reference,
                                             const fovis::OdometryFrame* target,
                                             const Eigen::Isometry3d& motion_estimate = Eigen::Isometry3d::Identity());

  bool compute(fovis::OdometryFrame* reference, fovis::OdometryFrame* target, common::Transform& transform);

  fovis::OdometryFrame* reference_frame_;
  fovis::OdometryFrame* r_frame_;
  fovis::OdometryFrame* t_frame_;

  void prepareFrame(common::Frame::Ptr frame, fovis::OdometryFrame* fovis_frame, int fast_threshold);

  int getAdaptiveThreshold(const fovis::OdometryFrame* frame);

    //// === tuning parameters ===

    //int _feature_window_size;

    //int _num_pyramid_levels;

    //// initial feature detector threshold
  int fast_threshold_;
  int fast_threshold_new_;

    //// params for adaptive feature detector threshold
  double fast_threshold_adaptive_gain_;
  int fast_threshold_min_;
  int fast_threshold_max_;
  int target_num_features_;
  int target_pixels_per_feature_;



  bool use_adaptive_threshold_;
  bool use_homography_initialization_;

    //// Which level of the image pyramid to use for initial rotation estimation
    //int _initial_rotation_pyramid_level;

  fovis::VisualOdometryOptions options_;

};

}

#endif

