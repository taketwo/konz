#ifndef __KONZ_FOVIS_FOVIS_ODOMETRY_H
#define __KONZ_FOVIS_FOVIS_ODOMETRY_H

#include <string>

#include <fovis/fovis.hpp>
#include <konz_common/odometry.h>

namespace konz
{

class FovisOdometry : public common::Odometry
{

public:

  FovisOdometry(const std::string& config_filename, const sensor_msgs::CameraInfo::ConstPtr& camera_info);

  ~FovisOdometry();

  virtual void process(common::Frame::Ptr frame);

  virtual common::Transform getPose() const { return pose_; }

  virtual common::Transform getDeltaTransform() const { return delta_transform_; }

  virtual bool keyframeCreatedAtLastCall() const { return keyframe_; }

  virtual bool estimationFailedAtLastCall() const { return failure_; }

private:

  // Fovis-related members
  fovis::CameraIntrinsicsParameters rgb_params_;
  fovis::VisualOdometry* odometry_;
  fovis::Rectification* rectification_;
  fovis::VisualOdometryOptions options_;

  // Data storage
  fovis::DepthImage* depth_image_;
  float* depth_data_;

  // Transforms
  common::Transform pose_;
  common::Transform delta_transform_;

  // Flags
  bool keyframe_;
  bool failure_;

};

}

#endif

