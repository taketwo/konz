#ifndef __KONZ_FOVIS_OPTIONS_H
#define __KONZ_FOVIS_OPTIONS_H

#include <string>

#include <fovis/options.hpp>

namespace konz
{

/** Load visual odometry options from the ROS parameter server.
  *
  * This function first loads the default options (thereby creating all the
  * entries in the options map). Next for each entry it queries the ROS
  * parameter server if the corresponding parameter exists, and loads it if
  * this is the case.
  *
  * Optionally a namespace is prepended to each parameter name. */
fovis::VisualOdometryOptions loadOptionsFromParameterServer(const std::string& ns = "");

/** Load visual odometry options from a Yaml file.
  *
  * This function first loads the default options (thereby creating all the
  * entries in the options map). It then iterates over the entries in the Yaml
  * file and whenever a corresponding entry exists in the options map it
  * replaces the default value with the value from the file. */
fovis::VisualOdometryOptions loadOptionsFromYaml(const std::string& filename);

}

#endif

