#include <konz_common/transform_filters/null_transform_filter.h>

#include "konz_fovis/fovis_odometry.h"
#include "konz_fovis/conversions.h"
#include "konz_fovis/options.h"

namespace konz
{

FovisOdometry::FovisOdometry(const std::string& config_filename, const sensor_msgs::CameraInfo::ConstPtr& camera_info)
: pose_(common::Transform::Identity())
, delta_transform_(common::Transform::Identity())
, keyframe_(false)
, failure_(false)
{
  options_ = loadOptionsFromYaml(config_filename);
  cameraInfoMsgToCameraIntrinsicsParameters(camera_info, rgb_params_);
  rectification_ = new fovis::Rectification(rgb_params_);
  odometry_ = new fovis::VisualOdometry(rectification_, options_);
  depth_image_ = new fovis::DepthImage(rgb_params_, rgb_params_.width, rgb_params_.height);
  depth_data_ = new float[rgb_params_.width * rgb_params_.height];
  transform_filter_ = std::make_shared<common::transform_filters::NullTransformFilter>();
}

FovisOdometry::~FovisOdometry()
{
  delete odometry_;
  delete rectification_;
  delete depth_image_;
  delete[] depth_data_;
}

void FovisOdometry::process(common::Frame::Ptr frame)
{
  cv::Mat gray;
  colorImageMsgToCvMat(frame->getColorImage(), gray);
  depthImageMsgToDepthData(frame->getDepthImage(), depth_data_);
  depth_image_->setDepthImage(depth_data_);

  odometry_->processFrame(gray.data, depth_image_);

  auto status = odometry_->getMotionEstimateStatus();
  failure_ = (status != ::fovis::SUCCESS && status != ::fovis::NO_DATA);
  keyframe_ = odometry_->getChangeReferenceFrames();
  delta_transform_ = transform_filter_->apply(odometry_->getMotionEstimate().cast<common::Scalar>());
  pose_ = pose_ * delta_transform_;
}

}

