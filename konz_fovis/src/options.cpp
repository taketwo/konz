#include <algorithm>
#include <iostream>
#include <fstream>

#include <fovis/fovis.hpp>
#include <yaml-cpp/yaml.h>
#include <ros/ros.h>

#include "konz_fovis/options.h"

namespace konz
{

fovis::VisualOdometryOptions loadOptionsFromParameterServer(const std::string& ns)
{
  typedef fovis::VisualOdometryOptions::iterator iterator;
  fovis::VisualOdometryOptions options = fovis::VisualOdometry::getDefaultOptions();
  for (iterator i = options.begin(); i != options.end(); i++)
  {
    std::string uri = ns + i->first;
    // fovis uses '-' as a separator in the option names, whereas ROS uses '_'
    std::replace(uri.begin(), uri.end(), '-', '_');
    if (ros::param::has(uri))
      ros::param::get(uri, i->second);
  }
  return options;
}

fovis::VisualOdometryOptions loadOptionsFromYaml(const std::string& filename)
{
  typedef fovis::VisualOdometryOptions::iterator iterator;
  fovis::VisualOdometryOptions options = fovis::VisualOdometry::getDefaultOptions();
  std::ifstream config_file(filename.c_str());
  if (config_file.good())
  {
    try
    {
      YAML::Parser parser(config_file);
      YAML::Node doc;
      parser.GetNextDocument(doc);
      for (YAML::Iterator it = doc.begin(); it != doc.end(); it++)
      {
        std::string key;
        it.first() >> key;
        std::replace(key.begin(), key.end(), '_', '-');
        try
        {
          it.second() >> options.at(key);
        }
        catch (std::out_of_range& e)
        {
          // this key is not a valid fovis option, ignore it
        }
      }
    }
    catch (YAML::Exception& e)
    {
      throw std::runtime_error("failed to parse YAML config file");
    }
  }
  else
  {
    throw std::runtime_error("failed to open YAML config file");
  }
  return options;
}

}

