//#include <iostream>
//#include <fstream>
#include <fovis/internal_utils.hpp>
#include <fovis/initial_homography_estimation.hpp>

#include "konz_fovis/fovis_transform_computer.h"
#include "konz_fovis/conversions.h"
#include "konz_fovis/options.h"

namespace konz
{

FovisTransformComputer::FovisTransformComputer(const std::string& config_filename, const sensor_msgs::CameraInfo::ConstPtr& camera_info)
{
  options_ = loadOptionsFromYaml(config_filename);
  cameraInfoMsgToCameraIntrinsicsParameters(camera_info, rgb_params_);
  rectification_ = new fovis::Rectification(rgb_params_);
  /*

  // extract options
  _feature_window_size = optionsGetIntOrFromDefault(_options, "feature-window-size", defaults);
  _num_pyramid_levels = optionsGetIntOrFromDefault(_options, "max-pyramid-level", defaults);
  */
  fovis::optionsGetInt(options_, "fast-threshold", &fast_threshold_);
  fovis::optionsGetBool(options_, "use-adaptive-threshold", &use_adaptive_threshold_);
  fovis::optionsGetDouble(options_, "fast-threshold-adaptive-gain", &fast_threshold_adaptive_gain_);
  fovis::optionsGetInt(options_, "target-pixels-per-feature", &target_pixels_per_feature_);
  fovis::optionsGetBool(options_, "use-homography-initialization", &use_homography_initialization_);

  fast_threshold_min_ = 5;
  fast_threshold_max_ = 70;

  target_num_features_ = rgb_params_.width * rgb_params_.height / target_pixels_per_feature_;

  reference_frame_ = new fovis::OdometryFrame(rectification_, options_);
  r_frame_ = new fovis::OdometryFrame(rectification_, options_);
  t_frame_ = new fovis::OdometryFrame(rectification_, options_);
  estimator_ = new fovis::MotionEstimator(rectification_, options_);
  depth_image_ = new fovis::DepthImage(rgb_params_, rgb_params_.width, rgb_params_.height);
  depth_data_ = new float[rgb_params_.width * rgb_params_.height];
}

FovisTransformComputer::~FovisTransformComputer()
{
  delete reference_frame_;
  delete r_frame_;
  delete t_frame_;
  delete estimator_;
  delete rectification_;
  delete depth_image_;
  delete[] depth_data_;
}

bool FovisTransformComputer::compute(fovis::OdometryFrame* reference, fovis::OdometryFrame* target, common::Transform& transform)
{
  //Eigen::Quaterniond rotation = Eigen::Quaterniond(1, 0, 0, 0);
  Eigen::Quaterniond rotation = use_homography_initialization_ ? estimateInitialRotation(reference, target) : Eigen::Quaterniond(1, 0, 0, 0);

  Eigen::Isometry3d motion_estimate = Eigen::Isometry3d::Identity();
  motion_estimate.rotate(rotation);

  Eigen::MatrixXd motion_cov;
  motion_cov.setIdentity();

  estimator_->estimateMotion(reference,
                             target,
                             depth_image_, // does not matter if the image actually changed, it only needs baseline (wich is anyways zero)
                             motion_estimate,
                             motion_cov);

  if(estimator_->isMotionEstimateValid())
  {
    transform = estimator_->getMotionEstimate().cast<common::Scalar>();
    //transform = estimator_->getMotionEstimate().inverse().cast<common::Scalar>();
    return true;
  }
  else
  {
    return false;
  }
}

bool FovisTransformComputer::compute(common::Frame::Ptr reference, common::Frame::Ptr target, common::Transform& transform)
{
  prepareFrame(reference, r_frame_, fast_threshold_);
  prepareFrame(target, t_frame_, use_adaptive_threshold_ ? getAdaptiveThreshold(r_frame_) : fast_threshold_);
  return compute(r_frame_, t_frame_, transform);
}

bool FovisTransformComputer::compute(common::Frame::Ptr target, common::Transform& transform)
{
  prepareFrame(target, t_frame_, use_adaptive_threshold_ ? getAdaptiveThreshold(reference_frame_) : fast_threshold_);
  return compute(reference_frame_, t_frame_, transform);
}

void FovisTransformComputer::setReferenceFrame(common::Frame::Ptr reference)
{
  prepareFrame(reference, reference_frame_, fast_threshold_);
}

void FovisTransformComputer::prepareFrame(common::Frame::Ptr frame, fovis::OdometryFrame* fovis_frame, int fast_threshold)
{
  cv::Mat gray;
  colorImageMsgToCvMat(frame->getColorImage(), gray);
  depthImageMsgToDepthData(frame->getDepthImage(), depth_data_);
  depth_image_->setDepthImage(depth_data_);
  fovis_frame->prepareFrame(gray.data, fast_threshold, depth_image_);
}

int FovisTransformComputer::getAdaptiveThreshold(const fovis::OdometryFrame* frame)
{
  // Compute new feature detector threshold based on how many features were
  // detected, use proportional control. We will pass this value outside to
  // whoever is interested in it.
  int error = frame->getNumDetectedKeypoints() - target_num_features_;
  int thresh_adjustment = (int)((error) * fast_threshold_adaptive_gain_);
  int new_thresh = fast_threshold_ + thresh_adjustment;
  if (new_thresh > fast_threshold_max_)
    return fast_threshold_max_;
  if (new_thresh < fast_threshold_min_)
    return fast_threshold_min_;
  return new_thresh;
}

// Estimate an initial rotation by finding the 2D homography that best aligns
// a template image (the previous image) with the current image.  From this
// homography, extract initial rotation parameters.
Eigen::Quaterniond FovisTransformComputer::estimateInitialRotation(const fovis::OdometryFrame* reference, const fovis::OdometryFrame* target, const Eigen::Isometry3d& motion_estimate)
{
  int initial_rotation_pyramid_level = 4;
  int num_pyr_levels = reference->getNumLevels();
  int pyrLevel = MIN(num_pyr_levels - 1, initial_rotation_pyramid_level);
  const fovis::PyramidLevel * ref_level = reference->getLevel(pyrLevel);
  const fovis::PyramidLevel * target_level = target->getLevel(pyrLevel);

  fovis::InitialHomographyEstimator rotation_estimator;
  rotation_estimator.setTemplateImage(ref_level->getGrayscaleImage(),
                                      ref_level->getWidth(),
                                      ref_level->getHeight(),
                                      ref_level->getGrayscaleImageStride(),
                                      initial_rotation_pyramid_level - pyrLevel);

  rotation_estimator.setTestImage(target_level->getGrayscaleImage(),
                                  target_level->getWidth(),
                                  target_level->getHeight(),
                                  target_level->getGrayscaleImageStride(),
                                  initial_rotation_pyramid_level - pyrLevel);

  Eigen::Matrix3f H = Eigen::Matrix3f::Identity();
  double finalRMS = 0;
  H = rotation_estimator.track(H,8, &finalRMS);
  double scale_factor = 1 << initial_rotation_pyramid_level;
  Eigen::Matrix3f S = Eigen::Matrix3f::Identity() * scale_factor;
  S(2, 2) = 1;
  //scale H up to the full size image
  H = S * H * S.inverse();

  Eigen::Vector3d rpy = Eigen::Vector3d::Zero();
  rpy(0) = asin(H(1, 2) / rgb_params_.fx);
  rpy(1) = -asin(H(0, 2) / rgb_params_.fx);
  rpy(2) = -atan2(H(1, 0), H(0, 0));

  Eigen::Quaterniond q = fovis::_rpy_to_quat(rpy);
  return q;
}

}

