#include <cv_bridge/cv_bridge.h>
#include <pcl/point_cloud.h>
#include <pcl/point_types.h>
#include <pcl_ros/point_cloud.h>
#include <sensor_msgs/image_encodings.h>

#include <konz_common/depth_traits.h>

#include "konz_fovis/conversions.h"

namespace konz
{

using common::DepthTraits;

void colorImageMsgToCvMat(const sensor_msgs::ImageConstPtr& image_msg, cv::Mat& mat)
{
  mat = cv_bridge::toCvCopy(image_msg, "mono8")->image;
}

void depthImageMsgToDepthData(const sensor_msgs::ImageConstPtr& image_msg, float* depth_data)
{
  size_t size = image_msg->width * image_msg->height;
  if (image_msg->encoding == sensor_msgs::image_encodings::TYPE_16UC1)
  {
    const uint16_t* data = reinterpret_cast<const uint16_t*>(&image_msg->data[0]);
    for (size_t i = 0; i < size; i++)
      depth_data[i] = DepthTraits<uint16_t>::valid(data[i]) ? DepthTraits<uint16_t>::toMeters(data[i]) : NAN;
  }
  else if (image_msg->encoding == sensor_msgs::image_encodings::TYPE_32FC1)
  {
    const float* data = reinterpret_cast<const float*>(&image_msg->data[0]);
    for (size_t i = 0; i < size; i++)
      depth_data[i] = DepthTraits<float>::valid(data[i]) ? DepthTraits<float>::toMeters(data[i]) : NAN;
  }
  else
  {
    ROS_ASSERT_MSG(false, "Depth image has unsupported encoding.");
  }
}

void cameraInfoMsgToCameraIntrinsicsParameters(const sensor_msgs::CameraInfoConstPtr& camera_info_msg, fovis::CameraIntrinsicsParameters& params)
{
  memset(&params, 0, sizeof(fovis::CameraIntrinsicsParameters));
  params.width = static_cast<int>(camera_info_msg->width);
  params.height = static_cast<int>(camera_info_msg->height);
  params.fx = camera_info_msg->K[0];
  params.fy = camera_info_msg->K[4];
  params.cx = camera_info_msg->K[2];
  params.cy = camera_info_msg->K[5];
  params.k1 = camera_info_msg->D[0];
  params.k2 = camera_info_msg->D[1];
  params.p1 = camera_info_msg->D[2];
  params.p2 = camera_info_msg->D[3];
  params.k3 = camera_info_msg->D[4];
}

void pointCloudMsgToCvMat(const sensor_msgs::PointCloud2ConstPtr& cloud_msg,
                          const fovis::CameraIntrinsicsParameters& params,
                          cv::Mat& mat)
{
  ROS_ASSERT_MSG((static_cast<int>(cloud_msg->width) == params.width && static_cast<int>(cloud_msg->height) == params.height),
                 "Cloud dimensions and the expected color image dimensions do not match.");
  sensor_msgs::ImagePtr image_ptr = boost::make_shared<sensor_msgs::Image>();
  try
  {
    pcl::toROSMsg(*cloud_msg, *image_ptr);
  }
  catch (std::runtime_error& e)
  {
    ROS_ERROR("Error in converting point cloud to image message: %s.", e.what());
  }
  mat = cv_bridge::toCvCopy(image_ptr, "mono8")->image;
}

void pointCloudMsgToDepthData(const sensor_msgs::PointCloud2ConstPtr& cloud_msg,
                              const fovis::CameraIntrinsicsParameters& params,
                              float* depth_data)
{
  ROS_ASSERT_MSG((static_cast<int>(cloud_msg->width) == params.width && static_cast<int>(cloud_msg->height) == params.height),
                 "Cloud dimensions and the expected depth image dimensions do not match.");
  // Convert pointcloud to PCL format
  pcl::PointCloud<pcl::PointXYZ> pcl_cloud;
  pcl::fromROSMsg(*cloud_msg, pcl_cloud);
  // Go through all point and project back
  for (size_t i = 0; i < pcl_cloud.points.size(); ++i)
  {
    if (!isnan(pcl_cloud.points[i].x))
    {
      float depth = pcl_cloud.points[i].z;
      int x = pcl_cloud.points[i].x * params.fx / depth + params.cx;
      int y = pcl_cloud.points[i].y * params.fy / depth + params.cy;
      ROS_ASSERT_MSG((x >= 0 && x < params.width), "X index out of bounds when re-projecting point.");
      ROS_ASSERT_MSG((y >= 0 && y < params.height), "Y index out of bounds when re-projecting point.");
      depth_data[y * params.width + x] = depth;
    }
    else
    {
      depth_data[i] = NAN;
    }
  }
}

}

