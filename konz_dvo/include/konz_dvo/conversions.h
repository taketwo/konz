#ifndef __KONZ_FOVIS_CONVERSIONS_H
#define __KONZ_FOVIS_CONVERSIONS_H

#include <opencv/cv.h>
#include <sensor_msgs/CameraInfo.h>
#include <sensor_msgs/Image.h>
#include <dvo/core/intrinsic_matrix.h>

namespace konz
{

void colorImageMsgToCvMat(const sensor_msgs::ImageConstPtr& image_msg, cv::Mat& mat);

void depthImageMsgToCvMat(const sensor_msgs::ImageConstPtr& image_msg, cv::Mat& mat);

void cameraInfoMsgToIntrinsicMatrix(const sensor_msgs::CameraInfoConstPtr& camera_info_msg,
                                    dvo::core::IntrinsicMatrix& intrinsics);

}

#endif

