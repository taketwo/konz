#ifndef __KONZ_DVO_DVO_ODOMETRY_H
#define __KONZ_DVO_DVO_ODOMETRY_H

#include <dvo/dense_tracking.h>
#include <konz_common/odometry.h>

namespace konz
{

class DVOOdometry : public common::Odometry
{

public:

  DVOOdometry(const std::string& config_filename, const sensor_msgs::CameraInfo::ConstPtr& camera_info);

  ~DVOOdometry();

  virtual void process(common::Frame::Ptr frame);

  virtual common::Transform getPose() const { return T_origin_keyframe_ * T_keyframe_current_; }

  virtual common::Transform getDeltaTransform() const { return T_keyframe_previous_.inverse() * T_keyframe_current_; }

  virtual bool keyframeCreatedAtLastCall() const { return keyframe_; }

  virtual bool estimationFailedAtLastCall() const { return !success_; }

private:

  bool transformSanityCheck(const Eigen::Affine3d transform);

  bool keyframeRequiredCheck(const Eigen::Affine3d transform);

  // DVO-related members
  std::unique_ptr<dvo::DenseTracker> dvo_;
  dvo::DenseTracker::Config config_;
  dvo::core::IntrinsicMatrix intrinsics_;

  // Data storage
  typedef std::shared_ptr<dvo::core::RgbdImagePyramid> RgbdImagePyramidPtr;
  RgbdImagePyramidPtr keyframe_frame_;
  RgbdImagePyramidPtr previous_frame_;
  RgbdImagePyramidPtr current_frame_;

  // Transforms
  common::Transform T_origin_keyframe_;
  common::Transform T_keyframe_previous_;
  common::Transform T_keyframe_current_;

  // Flags
  bool keyframe_;
  bool success_;
  bool enable_keyframes_;

};

}

#endif

