#ifndef __KONZ_DVO_DVO_TRANSFORM_COMPUTER_H
#define __KONZ_DVO_DVO_TRANSFORM_COMPUTER_H

#include <dvo/dense_tracking.h>
#include <konz_common/transform_computer.h>

namespace konz
{

class DVOTransformComputer : public common::TransformComputer
{

public:

  DVOTransformComputer(const std::string& config_filename, const sensor_msgs::CameraInfo::ConstPtr& camera_info);

  virtual ~DVOTransformComputer();

  virtual bool compute(common::Frame::Ptr reference, common::Frame::Ptr target, common::Transform& transform);

  virtual bool compute(common::Frame::Ptr target, common::Transform& transform);

  virtual void setReferenceFrame(common::Frame::Ptr reference);

private:

  typedef std::shared_ptr<dvo::core::RgbdImagePyramid> RgbdImagePyramidPtr;

  bool compute(RgbdImagePyramidPtr reference, RgbdImagePyramidPtr target, common::Transform& transform);

  RgbdImagePyramidPtr prepareFrame(common::Frame::Ptr frame);

  std::unique_ptr<dvo::DenseTracker> dvo_;
  dvo::DenseTracker::Config config_;
  RgbdImagePyramidPtr reference_frame_;

};

}

#endif

