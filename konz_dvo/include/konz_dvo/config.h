#ifndef __KONZ_DVO_CONFIG_H
#define __KONZ_DVO_CONFIG_H

#include <string>

#include <dvo/dense_tracking.h>

namespace konz
{

/** Load dvo config from a Yaml file.
  *
  * This function first loads the default config and then overwrites those
  * that are present in the Yaml file. */
dvo::DenseTracker::Config loadConfigFromYaml(const std::string& filename);

struct DVOOdometryConfig
{
  bool keyframes;
};

/** Load DVOOdometry config from a Yaml file.
  *
  * This is ugly, but this is life. */
DVOOdometryConfig loadOdometryConfigFromYaml(const std::string& filename);

}

#endif

