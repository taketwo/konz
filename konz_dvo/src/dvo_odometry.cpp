#include <cmath>
#include <konz_common/util/geometry.h>
#include <konz_common/transform_filters/null_transform_filter.h>

#include "konz_dvo/dvo_odometry.h"
#include "konz_dvo/conversions.h"
#include "konz_dvo/config.h"

namespace konz
{

DVOOdometry::DVOOdometry(const std::string& config_filename, const sensor_msgs::CameraInfo::ConstPtr& camera_info)
: T_origin_keyframe_(common::Transform::Identity())
, T_keyframe_previous_(common::Transform::Identity())
, T_keyframe_current_(common::Transform::Identity())
, keyframe_(false)
, success_(true)
, enable_keyframes_(true)
{
  cameraInfoMsgToIntrinsicMatrix(camera_info, intrinsics_);
  config_ = loadConfigFromYaml(config_filename);
  dvo_ = std::unique_ptr<dvo::DenseTracker>(new dvo::DenseTracker(intrinsics_, config_));
  auto c = loadOdometryConfigFromYaml(config_filename);
  enable_keyframes_ = c.keyframes;
  transform_filter_ = std::make_shared<common::transform_filters::NullTransformFilter>();
}

DVOOdometry::~DVOOdometry()
{
}

void DVOOdometry::process(common::Frame::Ptr frame)
{
  // Force keyframe
  if (!enable_keyframes_)
    keyframe_ = true;

  if (keyframe_)
  {
    keyframe_frame_.swap(current_frame_);
    T_origin_keyframe_ = T_origin_keyframe_ * T_keyframe_current_;
    T_keyframe_previous_ = T_keyframe_current_.inverse() * T_keyframe_previous_;
    T_keyframe_current_.setIdentity();
  }
  else
  {
    previous_frame_.swap(current_frame_);
    T_keyframe_previous_ = T_keyframe_current_;
  }

  cv::Mat intensity, depth;
  colorImageMsgToCvMat(frame->getColorImage(), intensity);
  depthImageMsgToCvMat(frame->getDepthImage(), depth);
  current_frame_ = std::make_shared<dvo::core::RgbdImagePyramid>(intensity, depth);

  if (!keyframe_frame_)
  {
    keyframe_ = true;
    return;
  }

  bool changed_reference_frames = keyframe_;
  keyframe_ = false;

  Eigen::Affine3d transform = Eigen::Affine3d::Identity();
  success_ = dvo_->match(*keyframe_frame_, *current_frame_, transform);
  success_ &= transformSanityCheck(transform);

  if (success_)
  {
    T_keyframe_current_ = transform_filter_->apply(transform.cast<common::Scalar>());
  }
  else if (!changed_reference_frames)
  {
    // if the motion estimation failed, then try estimating against the
    // previous frame if it's not the reference frame.
    success_ = dvo_->match(*previous_frame_, *current_frame_, transform);
    success_ &= transformSanityCheck(transform);
    if (success_)
    {
      T_keyframe_current_ = T_keyframe_previous_ * transform_filter_->apply(transform.cast<common::Scalar>());
      keyframe_ = true;
    }
  }

  // decide whether to insert a keyframe
  if (!success_ || keyframeRequiredCheck(transform))
  {
    keyframe_ = true;
  }
}

bool DVOOdometry::transformSanityCheck(const Eigen::Affine3d transform)
{
  double d = common::util::transformDistance(transform);
  double a = common::util::transformAngle(transform);
  return (d < 0.08 && a < 0.08);
}

bool DVOOdometry::keyframeRequiredCheck(const Eigen::Affine3d transform)
{
  double d = common::util::transformDistance(transform);
  double a = common::util::transformAngle(transform);
  return (d > 0.04 || a > 0.05);
}

}

