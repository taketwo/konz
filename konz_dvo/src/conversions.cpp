#include <cv_bridge/cv_bridge.h>
#include <dvo/core/surface_pyramid.h>

#include "konz_dvo/conversions.h"

namespace konz
{

void colorImageMsgToCvMat(const sensor_msgs::ImageConstPtr& image_msg, cv::Mat& mat)
{
  cv::Mat image = cv_bridge::toCvShare(image_msg)->image;
  if(image.channels() == 3)
  {
    cv::Mat tmp;
    cv::cvtColor(image, tmp, CV_BGR2GRAY, 1);
    tmp.convertTo(mat, CV_32F);
  }
  else
  {
    image.convertTo(mat, CV_32F);
  }
}

void depthImageMsgToCvMat(const sensor_msgs::ImageConstPtr& image_msg, cv::Mat& mat)
{
  cv::Mat image = cv_bridge::toCvShare(image_msg)->image;
  if(image.type() == CV_16UC1)
    dvo::core::SurfacePyramid::convertRawDepthImageSse(image, mat, 0.001);
  else
    mat = image.clone();
}

void cameraInfoMsgToIntrinsicMatrix(const sensor_msgs::CameraInfoConstPtr& camera_info_msg, dvo::core::IntrinsicMatrix& intrinsics)
{
  intrinsics = dvo::core::IntrinsicMatrix::create(camera_info_msg->P[0],
                                                  camera_info_msg->P[5],
                                                  camera_info_msg->P[2],
                                                  camera_info_msg->P[6]);
}

}

