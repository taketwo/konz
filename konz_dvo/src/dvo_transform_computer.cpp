#include "konz_dvo/dvo_transform_computer.h"
#include "konz_dvo/conversions.h"
#include "konz_dvo/config.h"

namespace konz
{

DVOTransformComputer::DVOTransformComputer(const std::string& config_filename, const sensor_msgs::CameraInfo::ConstPtr& camera_info)
{
  dvo::core::IntrinsicMatrix intrinsics;
  cameraInfoMsgToIntrinsicMatrix(camera_info, intrinsics);
  config_ = loadConfigFromYaml(config_filename);
  dvo_ = std::unique_ptr<dvo::DenseTracker>(new dvo::DenseTracker(intrinsics, config_));
}

DVOTransformComputer::~DVOTransformComputer()
{
}

bool DVOTransformComputer::compute(RgbdImagePyramidPtr reference, RgbdImagePyramidPtr target, common::Transform& transform)
{
  Eigen::Affine3d t = Eigen::Affine3d::Identity();
  bool result = dvo_->match(*reference, *target, t);
  transform = t.cast<common::Scalar>();
  return result;
}

bool DVOTransformComputer::compute(common::Frame::Ptr reference, common::Frame::Ptr target, common::Transform& transform)
{
  return compute(prepareFrame(reference), prepareFrame(target), transform);
}

bool DVOTransformComputer::compute(common::Frame::Ptr target, common::Transform& transform)
{
  return compute(reference_frame_, prepareFrame(target), transform);
}

void DVOTransformComputer::setReferenceFrame(common::Frame::Ptr reference)
{
  reference_frame_ = prepareFrame(reference);
}

DVOTransformComputer::RgbdImagePyramidPtr DVOTransformComputer::prepareFrame(common::Frame::Ptr frame)
{
  cv::Mat intensity, depth;
  colorImageMsgToCvMat(frame->getColorImage(), intensity);
  depthImageMsgToCvMat(frame->getDepthImage(), depth);
  return std::make_shared<dvo::core::RgbdImagePyramid>(intensity, depth);
}

}

