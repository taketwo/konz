#include <algorithm>
#include <iostream>
#include <fstream>

#include <yaml-cpp/yaml.h>
#include <dvo/dense_tracking.h>
#include <dvo/core/datatypes.h>
#include <dvo/core/weight_calculation.h>

#include "konz_dvo/config.h"

namespace konz
{

dvo::DenseTracker::Config loadConfigFromYaml(const std::string& filename)
{
  dvo::DenseTracker::Config config;
  std::ifstream config_file(filename.c_str());
  if (config_file.good())
  {
    try
    {
      YAML::Parser parser(config_file);
      YAML::Node doc;
      parser.GetNextDocument(doc);
      if (doc.FindValue("first_level"))
        doc["first_level"] >> config.FirstLevel;
      if (doc.FindValue("last_level"))
        doc["last_level"] >> config.LastLevel;
      if (doc.FindValue("max_iterations_per_level"))
        doc["max_iterations_per_level"] >> config.MaxIterationsPerLevel;
      if (doc.FindValue("precision"))
        doc["precision"] >> config.Precision;
      if (doc.FindValue("lambda"))
        doc["lambda"] >> config.Lambda;
      if (doc.FindValue("mu"))
        doc["mu"] >> config.Mu;
      if (doc.FindValue("use_initial_estimate"))
        doc["use_initial_estimate"] >> config.UseInitialEstimate;
      if (doc.FindValue("use_weighting"))
        doc["use_weighting"] >> config.UseWeighting;
      if (doc.FindValue("scale_estimator_param"))
        doc["scale_estimator_param"] >> config.ScaleEstimatorParam;
      if (doc.FindValue("influence_function_param"))
        doc["influence_function_param"] >> config.InfluenceFunctionParam;
      if (doc.FindValue("influence_function_type"))
      {
        std::string value;
        doc["influence_function_type"] >> value;
        if(value == "Tukey")
          config.InfluenceFuntionType  = dvo::core::InfluenceFunctions::Tukey;
        if(value == "TDistribution")
          config.InfluenceFuntionType  = dvo::core::InfluenceFunctions::TDistribution;
        if(value == "Huber")
          config.InfluenceFuntionType  = dvo::core::InfluenceFunctions::Huber;
      }
      if (doc.FindValue("scale_estimator_type"))
      {
        std::string value;
        doc["scale_estimator_type"] >> value;
        if(value == "NormalDistribution")
          config.ScaleEstimatorType = dvo::core::ScaleEstimators::NormalDistribution;
        if(value == "TDistribution")
          config.ScaleEstimatorType = dvo::core::ScaleEstimators::TDistribution;
        if(value == "MAD")
          config.ScaleEstimatorType = dvo::core::ScaleEstimators::MAD;
      }
    }
    catch (YAML::Exception& e)
    {
      throw std::runtime_error("failed to parse YAML config file");
    }
  }
  else
  {
    throw std::runtime_error("failed to open YAML config file");
  }
  return config;
}

DVOOdometryConfig loadOdometryConfigFromYaml(const std::string& filename)
{
  DVOOdometryConfig config;
  config.keyframes = true;
  std::ifstream config_file(filename.c_str());
  if (config_file.good())
  {
    try
    {
      YAML::Parser parser(config_file);
      YAML::Node doc;
      parser.GetNextDocument(doc);
      if (doc.FindValue("keyframes"))
        doc["keyframes"] >> config.keyframes;
    }
    catch (YAML::Exception& e)
    {
      throw std::runtime_error("failed to parse YAML config file");
    }
  }
  else
  {
    throw std::runtime_error("failed to open YAML config file");
  }
  return config;
}

}

