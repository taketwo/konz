#include <iostream>

#include <konz_common/nodes/transform_computer_evaluation.h>

#include "konz_dvo/dvo_transform_computer.h"

using namespace konz;
using namespace konz::common;
using namespace konz::common::nodes;

class Evaluation : public TransformComputerEvaluation
{

protected:

  virtual void initTransformComputer()
  {
    transform_computer_ = TransformComputer::UPtr(new DVOTransformComputer(args_.config, emitter_->getCameraInfo()));
  }

};

int main(int argc, char* argv[])
{
  try
  {
    Evaluation eval;
    if (eval.init(argc, argv))
    {
      std::cout << "Initialization succeded.\n";
      eval.run();
    }
    return 0;
  }
  catch(std::exception& e)
  {
    std::cerr << "Transform computer evaluation failed: " << e.what() << std::endl;
    return 1;
  }
}

