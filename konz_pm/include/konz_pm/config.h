#ifndef __KONZ_PM_CONFIG_H
#define __KONZ_PM_CONFIG_H

#include <string>

namespace konz
{

struct PMConfig
{
  std::string icp_config;
  std::string keyframe_filters_config;
  int downsampling_level;
  PMConfig(): downsampling_level(0) { }
};

PMConfig loadConfigFromYaml(const std::string& filename);

}

#endif

