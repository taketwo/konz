#ifndef __KONZ_PM_PM_TRANSFORM_COMPUTER_H
#define __KONZ_PM_PM_TRANSFORM_COMPUTER_H

#include <pointmatcher/PointMatcher.h>
#include <konz_common/transform_computer.h>

namespace konz
{

class PMTransformComputer : public common::TransformComputer
{

public:

  PMTransformComputer(const std::string& config_filename);

  virtual bool compute(common::Frame::Ptr reference, common::Frame::Ptr target, common::Transform& transform);

  virtual bool compute(common::Frame::Ptr target, common::Transform& transform);

  virtual void setReferenceFrame(common::Frame::Ptr reference);

private:

  typedef PointMatcher<common::Scalar> PM;
  typedef PM::DataPoints Cloud;
  typedef std::shared_ptr<Cloud> CloudPtr;

  bool compute(CloudPtr reference, CloudPtr target, common::Transform& transform);

  CloudPtr reference_cloud_;

  PM::ICP icp_;
  PM::DataPointsFilters reference_filters_;

  int downsampling_level_;

};

}

#endif

