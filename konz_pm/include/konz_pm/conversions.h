#ifndef __KONZ_PM_CONVERSIONS_H
#define __KONZ_PM_CONVERSIONS_H

#include <sensor_msgs/CameraInfo.h>
#include <sensor_msgs/Image.h>

#include <pointmatcher/PointMatcher.h>

namespace konz
{

typedef typename PointMatcher<float>::DataPoints PointMatcherCloud;
typedef std::shared_ptr<PointMatcherCloud> PointMatcherCloudPtr;

PointMatcherCloudPtr rosMsgsToPointMatcherCloudPtr(const sensor_msgs::ImageConstPtr& depth_msg,
                                                   const sensor_msgs::CameraInfoConstPtr& info_msg,
                                                   int downsampling_level = 0);

}

#endif

