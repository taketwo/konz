#ifndef __KONZ_PM_PM_ODOMETRY_H
#define __KONZ_PM_PM_ODOMETRY_H

#include <pointmatcher/PointMatcher.h>
#include <konz_common/odometry.h>

namespace konz
{

class PMOdometry : public common::Odometry
{

public:

  PMOdometry(const std::string& config_filename);

  ~PMOdometry();

  virtual void process(common::Frame::Ptr frame);

  virtual common::Transform getPose() const { return T_origin_keyframe_ * T_keyframe_current_; }

  virtual common::Transform getDeltaTransform() const { return T_keyframe_previous_.inverse() * T_keyframe_current_; }

  virtual bool keyframeCreatedAtLastCall() const { return keyframe_; }

  virtual bool estimationFailedAtLastCall() const { return failure_; }

private:

  typedef PointMatcher<common::Scalar> PM;
  typedef typename PM::DataPoints Cloud;

  void createKeyframe(Cloud& cloud);

  static inline common::Transform matrixToTransform(const common::Matrix& matrix)
  {
    common::Transform transform;
    transform.matrix() = matrix;
    return transform;
  }

  // PointMatcher-related members
  PM::ICPSequence icp_;
  PM::DataPointsFilters keyframe_filters_;

  // Transforms
  common::Transform T_origin_keyframe_;
  common::Transform T_keyframe_previous_;
  common::Transform T_keyframe_current_;
  common::Transform estimated_transform_;

  // Flags
  bool keyframe_;
  bool failure_;
  int downsampling_level_;

};

}

#endif

