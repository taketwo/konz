#include <iostream>
#include <fstream>

#include <boost/filesystem.hpp>
#include <yaml-cpp/yaml.h>

#include "konz_pm/config.h"

namespace konz
{

PMConfig loadConfigFromYaml(const std::string& filename)
{
  auto p = boost::filesystem::absolute(filename).remove_filename();
  PMConfig config;
  std::ifstream config_file(filename.c_str());
  if (config_file.good())
  {
    try
    {
      YAML::Parser parser(config_file);
      YAML::Node doc;
      parser.GetNextDocument(doc);
      if (doc.FindValue("icp_config"))
      {
        std::string fname;
        doc["icp_config"] >> fname;
        config.icp_config = (p / fname).string();
      }
      if (doc.FindValue("keyframe_filters_config"))
      {
        std::string fname;
        doc["keyframe_filters_config"] >> fname;
        config.keyframe_filters_config = (p / fname).string();
      }
      if (doc.FindValue("downsampling_level"))
        doc["downsampling_level"] >> config.downsampling_level;
    }
    catch (YAML::Exception& e)
    {
      throw std::runtime_error("failed to parse YAML config file");
    }
  }
  else
  {
    throw std::runtime_error("failed to open YAML config file");
  }
  return config;
}

}

