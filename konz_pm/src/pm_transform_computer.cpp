#include <iostream>
#include <fstream>
#include <string>

#include "konz_pm/pm_transform_computer.h"
#include "konz_pm/conversions.h"
#include "konz_pm/config.h"

namespace konz
{

PMTransformComputer::PMTransformComputer(const std::string& config_filename)
: downsampling_level_(0)
{
  PMConfig config = loadConfigFromYaml(config_filename);
  downsampling_level_ = config.downsampling_level;

  // Load ICP chain configuration
  std::ifstream icfs(config.icp_config);
  if (icfs.good())
  {
    icp_.loadFromYaml(icfs);
  }
  else
  {
    std::cerr << "An error occurred while trying to open ICP chain configuration file: " << config.icp_config << std::endl;
    icp_.setDefault();
  }

  // Load reference reading filter configuration
  std::ifstream kcfs(config.keyframe_filters_config);
  if (!kcfs.good())
    std::cerr << "An error occurred while trying to open ICP keyframe filters configuration file: " << config.keyframe_filters_config << std::endl;
  else
    reference_filters_ = PM::DataPointsFilters(kcfs);
}

bool PMTransformComputer::compute(CloudPtr reference, CloudPtr target, common::Transform& transform)
{
  transform = common::Transform::Identity();

  if (reference->features.rows() != target->features.rows() ||
      reference->features.cols() == 0 ||
      target->features.cols() == 0)
    return false;

  try
  {
    transform.matrix() = icp_(*target, *reference);
    return true;
  }
  catch (PM::ConvergenceError error)
  {
    return false;
  }
}

bool PMTransformComputer::compute(common::Frame::Ptr reference, common::Frame::Ptr target, common::Transform& transform)
{
  auto reference_cloud = rosMsgsToPointMatcherCloudPtr(reference->getDepthImage(), reference->getCameraInfo(), downsampling_level_);
  reference_filters_.apply(*reference_cloud);
  return compute(reference_cloud,
                 rosMsgsToPointMatcherCloudPtr(target->getDepthImage(), target->getCameraInfo(), downsampling_level_),
                 transform);
}

bool PMTransformComputer::compute(common::Frame::Ptr target, common::Transform& transform)
{
  return compute(reference_cloud_,
                 rosMsgsToPointMatcherCloudPtr(target->getDepthImage(), target->getCameraInfo(), downsampling_level_),
                 transform);
}

void PMTransformComputer::setReferenceFrame(common::Frame::Ptr reference)
{
  reference_cloud_ = rosMsgsToPointMatcherCloudPtr(reference->getDepthImage(), reference->getCameraInfo(), downsampling_level_);
  reference_filters_.apply(*reference_cloud_);
}

}

