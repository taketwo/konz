#include <iostream>
#include <fstream>

#include <yaml-cpp/yaml.h>
#include <konz_common/util/geometry.h>
#include <konz_common/transform_filters/null_transform_filter.h>

#include "konz_pm/pm_odometry.h"
#include "konz_pm/conversions.h"
#include "konz_pm/config.h"

namespace konz
{

PMOdometry::PMOdometry(const std::string& config_filename)
: T_origin_keyframe_(common::Transform::Identity())
, T_keyframe_previous_(common::Transform::Identity())
, T_keyframe_current_(common::Transform::Identity())
, keyframe_(false)
, failure_(false)
, downsampling_level_(0)
{
  PMConfig config = loadConfigFromYaml(config_filename);
  downsampling_level_ = config.downsampling_level;

  std::ifstream icfs(config.icp_config);
  if (icfs.good())
  {
    icp_.loadFromYaml(icfs);
  }
  else
  {
    std::cerr << "An error occurred while trying to open ICP chain configuration file: " << config.icp_config << std::endl;
    icp_.setDefault();
  }

  std::ifstream kcfs(config.keyframe_filters_config);
  if (!kcfs.good())
    std::cerr << "An error occurred while trying to open ICP keyframe filters configuration file: " << config.keyframe_filters_config << std::endl;
  else
    keyframe_filters_ = PM::DataPointsFilters(kcfs);

  transform_filter_ = std::make_shared<common::transform_filters::NullTransformFilter>();
}

PMOdometry::~PMOdometry()
{
}

void PMOdometry::process(common::Frame::Ptr frame)
{
  auto cloud = rosMsgsToPointMatcherCloudPtr(frame->getDepthImage(), frame->getCameraInfo(), downsampling_level_);

  failure_ = false;
  keyframe_ = false;

  // Check the number of points in the cloud
  const size_t point_count = cloud->features.cols();
  if (point_count < 2000)
  {
    failure_ = true;
    return;
  }

  // Initialize the map if empty
  if(!icp_.hasMap())
  {
    createKeyframe(*cloud);
    return;
  }

  T_keyframe_previous_ = T_keyframe_current_;

  try
  {
    estimated_transform_ = matrixToTransform(icp_(*cloud, estimated_transform_.matrix()));
    T_keyframe_current_ = transform_filter_->apply(estimated_transform_);

    double a = common::util::transformAngle(T_keyframe_current_);
    double d = common::util::transformDistance(T_keyframe_current_);

    if (d > 0.2 || a > 0.2)
    {
      failure_ = true;
      T_keyframe_current_ = T_keyframe_previous_;
      createKeyframe(*cloud);
    }
    else if (icp_.errorMinimizer->getOverlap() < 0.75)
    {
      createKeyframe(*cloud);
    }
  }
  catch (PM::ConvergenceError& error)
  {
    failure_ = true;
    createKeyframe(*cloud);
  }
}

void PMOdometry::createKeyframe(Cloud& points)
{
  T_origin_keyframe_ = T_origin_keyframe_ * T_keyframe_current_;
  T_keyframe_previous_ = T_keyframe_current_.inverse() * T_keyframe_previous_;
  T_keyframe_current_ = common::Transform::Identity();
  estimated_transform_ = common::Transform::Identity();
  keyframe_ = true;
  keyframe_filters_.apply(points);
  icp_.setMap(points);
}

}

