#include <image_geometry/pinhole_camera_model.h>
#include <cv_bridge/cv_bridge.h>

#include <konz_common/depth_traits.h>

#include "konz_pm/conversions.h"

namespace konz
{

using common::DepthTraits;

struct Intrinsics
{
  double cx;
  double cy;
  double fx;
  double fy;
};

template<typename T>
size_t countValidPoints(const cv::Mat& depth)
{
  const T* d = depth.ptr<T>();
  size_t size = depth.rows * depth.cols;
  size_t valid_count = 0;
  for (size_t i = 0; i < size; i++)
    if (DepthTraits<T>::valid(d[i]))
      valid_count++;
  return valid_count;
}

template<typename T>
void fillCloud(PointMatcherCloud& cloud, const cv::Mat& depth, Intrinsics& camera)
{
  PointMatcherCloud::View vx(cloud.getFeatureViewByName("x"));
  PointMatcherCloud::View vy(cloud.getFeatureViewByName("y"));
  PointMatcherCloud::View vz(cloud.getFeatureViewByName("z"));
  const T* d = depth.ptr<T>();
  int index = 0, j = 0;
  for (int v = 0; v < depth.rows; v++)
  {
    for (int u = 0; u < depth.cols; u++, j++)
    {
      if (DepthTraits<T>::valid(d[j]))
      {
        float depth = DepthTraits<T>::toMeters(d[j]);
        vx(0, index) = (u - camera.cx) * depth / camera.fx;
        vy(0, index) = (v - camera.cy) * depth / camera.fy;
        vz(0, index) = depth;
        index++;
      }
    }
  }
  cloud.getFeatureViewByName("pad").setConstant(1);
}

PointMatcherCloudPtr rosMsgsToPointMatcherCloudPtr(const sensor_msgs::ImageConstPtr& depth_msg, const sensor_msgs::CameraInfoConstPtr& info_msg, int downsampling_level)
{
  if (depth_msg && info_msg)
  {
    PointMatcherCloud::Labels feature_labels;
    feature_labels.push_back(PointMatcherCloud::Label("x", 1));
    feature_labels.push_back(PointMatcherCloud::Label("y", 1));
    feature_labels.push_back(PointMatcherCloud::Label("z", 1));
    feature_labels.push_back(PointMatcherCloud::Label("pad", 1));
    image_geometry::PinholeCameraModel camera;
    camera.fromCameraInfo(info_msg);
    Intrinsics c {camera.cx(), camera.cy(), camera.fx(), camera.fy()};
    cv::Mat depth;
    camera.rectifyImage(cv_bridge::toCvShare(depth_msg)->image, depth);

    while (downsampling_level-- > 0)
    {
      cv::Mat depth_ds;
      pyrDown(depth, depth_ds, cv::Size(depth.cols / 2, depth.rows / 2));
      c = {c.cx / 2, c.cy / 2, c.fx / 2, c.fy / 2};
      depth = depth_ds;
    }

    if (depth.type() == CV_16UC1)
    {
      auto cloud = std::make_shared<PointMatcherCloud>(feature_labels, PointMatcherCloud::Labels(), countValidPoints<uint16_t>(depth));
      fillCloud<uint16_t>(*cloud, depth, c);
      return cloud;
    }
    if (depth.type() == CV_32FC1)
    {
      auto cloud = std::make_shared<PointMatcherCloud>(feature_labels, PointMatcherCloud::Labels(), countValidPoints<float>(depth));
      fillCloud<float>(*cloud, depth, c);
      return cloud;
    }
  }
  return PointMatcherCloudPtr(new PointMatcherCloud);
}

}

