#include <iostream>

#include <konz_common/nodes/odometry_evaluation.h>

#include "konz_pm/pm_odometry.h"

using namespace konz;
using namespace konz::common;
using namespace konz::common::nodes;

class Evaluation : public OdometryEvaluation
{

protected:

  virtual void initOdometry()
  {
    odometry_ = Odometry::UPtr(new PMOdometry(args_.config));
  }

};

int main(int argc, char* argv[])
{
  try
  {
    Evaluation eval;
    if (eval.init(argc, argv))
    {
      std::cout << "Initialization succeded.\n";
      eval.run();
    }
    return 0;
  }
  catch(std::exception& e)
  {
    std::cerr << "Odometry evaluation failed: " << e.what() << std::endl;
    return 1;
  }
}

