import sys
import numpy as np
from subprocess import check_output

from transform import Transform


class ErrorsFile(file):

    def write_header(self, method, bagfile):
        file.write(self, '# errors of (%s) transform computation\n' % method)
        file.write(self, '# file: "%s"\n' % bagfile)
        file.write(self, '# format: gtd gta etd eta dd da ed ea tde rt fail\n')
        file.write(self, '# where:\n')
        file.write(self, '# gtd, gta - ground truth distance and angle\n')
        file.write(self, '# etd, eta - estimated distance and angle\n')
        file.write(self, '# dd, da - difference in distance and angle\n')
        file.write(self, '# ed, ea - error distance and angle\n')
        file.write(self, '# tde - translation direction error\n')
        file.write(self, '# rt, fail - runtime and computation failure flag\n')

    def write_line(self, gt, et, rt, fail):
        gt_vec, et_vec = gt.m[0:3, 3], et.m[0:3, 3]
        gt_vec_n, et_vec_n = np.linalg.norm(gt_vec), np.linalg.norm(et_vec)
        if gt_vec_n * et_vec_n < 0.000001:
            dir_error = 0
        else:
            dir_error = np.arccos(np.dot(et_vec, gt_vec) / gt_vec_n / et_vec_n)
        error = gt.transform_to(et)
        file.write(self, '%.4f %.4f %.4f %.4f %.4f %.4f %.4f %.4f %.4f %f %s\n'
                         % (gt_vec_n, gt.angle(),
                            et_vec_n, et.angle(),
                            abs(gt_vec_n - et_vec_n),
                            abs(gt.angle() - et.angle()),
                            error.distance(), error.angle(),
                            dir_error,
                            rt, '1' if fail else '0'))


class TransformsFile(file):

    @classmethod
    def parse_line(cls, line):
        line = line.replace(',', ' ').replace('\t', ' ')
        if not line or line[0] == '#':
            return
        tokens = [v.strip() for v in line.split(' ')]
        n = [float(t) for t in tokens if t != '' and t != '#']
        if len(n) != 10 or n[5:9] == [0, 0, 0, 0]:
            return
        if any(np.isnan(v) for v in n):
            sys.stderr.write('Warning: line has NaNs, skipping line.\n')
            return
        ts1, ts2, rt = n[0], n[1], n[9]
        n[5], n[6], n[7], n[8] = n[8], n[5], n[6], n[7]
        et = Transform.from_translation_and_rotation(n[2:5], n[5:9])
        return ts1, ts2, et, rt, '#' not in line


class TrajectoryFile(file):

    def write_header(self, title, bagfile):
        file.write(self, '# %s\n' % title)
        file.write(self, '# file: "%s"\n' % bagfile)
        file.write(self, '# timestamp tx ty tz qx qy qz qw\n')

    def write_line(self, timestamp, transform):
        if isinstance(transform, tuple) and len(transform) == 2:
            t, q = transform
        else:
            t, q = transform.translation, transform.quaternion
        file.write(self, '%.4f %.4f %.4f %.4f %.4f %.4f %.4f %.4f\n'
                         % (timestamp,
                            t[0], t[1], t[2],
                            q[1], q[2], q[3], q[0]))


class IntervalsFile(file):

    def write_header(self, md, ma, bagfile):
        file.write(self, '# intervals with max distance = %.3f m and max angle'
                         ' = %.4f rad\n' % (md, ma))
        file.write(self, '# file: "%s"\n' % bagfile)
        file.write(self, '# timestamp1 timestamp2\n')

    def write_line(self, timestamp1, timestamp2):
        file.write(self, '%.4f %.4f\n' % (timestamp1, timestamp2))


def get_line_count(filename):
    return int(check_output(['wc', '-l', filename]).split()[0])
