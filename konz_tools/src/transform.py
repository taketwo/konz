from math import degrees
import numpy as np
from transformations import (translation_matrix, quaternion_matrix,
                             quaternion_from_matrix, translation_from_matrix,
                             quaternion_slerp, euler_from_matrix)


class Transform:
    def __init__(self, m):
        self.m = m

    def inverse(self):
        return Transform(np.linalg.inv(self.m))

    def transform_to(self, b):
        return Transform(np.dot(np.linalg.inv(self.m), b.m))

    def distance(self):
        return np.linalg.norm(self.m[0:3, 3])

    def angle(self):
        return np.arccos(min(1, max(-1, (np.trace(self.m[0:3, 0:3]) - 1) / 2)))

    def __str__(self):
        e = [degrees(a) for a in self.euler]
        return 'Translation %s Rotation %s' % (self.translation, e)

    @property
    def quaternion(self):
        return quaternion_from_matrix(self.m)

    @property
    def euler(self):
        return euler_from_matrix(self.m[0:3, 0:3], 'sxyz')

    @property
    def translation(self):
        return translation_from_matrix(self.m)

    @property
    def x(self):
        return self.m[0, 3]

    @property
    def y(self):
        return self.m[1, 3]

    @property
    def z(self):
        return self.m[2, 3]

    @classmethod
    def interpolate(cls, a, b, alpha):
        qa, ta = quaternion_from_matrix(a.m), translation_from_matrix(a.m)
        qb, tb = quaternion_from_matrix(b.m), translation_from_matrix(b.m)
        qr = quaternion_slerp(qa, qb, alpha)
        tr = (1 - alpha) * ta + (alpha) * tb
        return Transform.from_translation_and_rotation(tr, qr)

    @classmethod
    def from_translation_and_rotation(cls, t, q):
        return Transform(np.dot(translation_matrix(t), quaternion_matrix(q)))

    @classmethod
    def from_translation(cls, t):
        return Transform(translation_matrix(t))

    @classmethod
    def from_rotation(cls, q):
        return Transform(quaternion_matrix(q))
