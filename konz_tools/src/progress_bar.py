#!/usr/bin/env python
# encoding: utf-8

import sys


class ProgressBar:

    def __init__(self, max_value, width):
        self.max_value = max_value
        self.width = width
        self.steps = 100.0 / width
        self.fmt = '\r[%s%s] %5.2f%%'
        self.last_value = -1
        self.update()

    def update(self, value=None):
        value = value or self.last_value + 1
        p = value * 100.0 / self.max_value
        p = 100 if p > 100 else p
        full = int(p / self.steps)
        empty = self.width - full
        sys.stdout.write(self.fmt % ('▪' * full, '▫' * empty, p))
        sys.stdout.flush()
        self.last_value = value

    def done(self):
        self.update(self.max_value)
        sys.stdout.write(' DONE\n')
