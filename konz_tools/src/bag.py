import re
import yaml
import os.path
import subprocess

import trajectory
from dataset import Dataset


class Bag:

    def __init__(self, filename):
        if not Bag.is_bag(filename):
            raise Exception('"%s" is not a bag file.' % filename)
        self.abspath = os.path.abspath(filename)
        self.basename = os.path.basename(filename)
        self.stem = os.path.splitext(self.basename)[0]
        self.dataset = Dataset.which_contains(filename)
        self.groundtruth = os.path.join(self.dataset.folders.groundtruth,
                                        '%s-groundtruth.txt' % self.stem)
        self.intervals = os.path.join(self.dataset.folders.intervals,
                                      '%s-intervals.txt' % self.stem)

    def count_messages_in_topic(self, topic):
        info = self.get_info()
        for d in info['topics']:
            if d['topic'] == topic:
                return d['messages']
        return 0

    def get_info(self):
        cmd = 'rosbag info --yaml %s' % self.abspath
        pipe = subprocess.Popen(cmd.split(), stdout=subprocess.PIPE)
        return yaml.load(pipe.communicate()[0])

    def load_groundtruth_trajectory(self):
        return trajectory.load(open(self.groundtruth, 'r'))

    @classmethod
    def is_bag(cls, filename):
        if os.path.isfile(filename) and re.search(r'\.bag$', filename):
            return True
        else:
            return False
