import sys
import numpy as np

from transform import Transform


class InterpolationError(Exception):
    def __init__(self):
        Exception.__init__(self, 'Calculation of the pose at requested '
                           'timestamp requires interpolation over an interval '
                           'that is larger than twice the median interval '
                           'between the poses in the trajectory.')


class ExtrapolationError(Exception):
    def __init__(self):
        Exception.__init__(self, 'Calculation of the pose at requested '
                           'timestamp requires extrapolation.')


class Trajectory:

    def __init__(self, poses, failures):
        self._p = poses
        self._f = failures
        self._ts = sorted(poses.keys())
        intervals = [t1 - t2 for t1, t2 in zip(self._ts[1:], self._ts[:-1])]
        self._interval = np.median(intervals)

    @property
    def timestamps(self):
        """
        A sorted list of the timestamps of all poses in the trajectory.
        """
        return self._ts

    @property
    def interval(self):
        """
        Median interval between adjacent timestamps in the trajectory.
        """
        return self._interval

    def at(self, ts, force_interpolation=False):
        """
        Get the pose at the specific timestamp. If the requested timestamp is
        "outside" of the trajectory, then ExtrapolationError is raised. If the
        requested timestamp does not coincide with any of the trajectory's
        timestamps (which will often be the case), then the function will
        attempt to interpolate the pose based on the two nearest poses. If the
        time difference with any of those nearest poses is greater than twice
        the median interval (see the documentation for the interval property),
        then InterpolationError will be raised.

        Parameters
        ----------
        ts : double
            The timestamp.
        force_interpolation: boolean
            Force interpolation even if the time difference with the nearest
            poses is too large.
        """
        if ts > self._ts[-1] or ts < self._ts[0]:
            raise ExtrapolationError()
        # Lucky case, direct hit
        if ts in self._ts:
            return self._p[ts]
        # Interpolation needed, find closest left and right timestamps
        ct, ci = self.find_closest_timestamp(ts)
        tl, tr = (ct, self._ts[ci + 1]) if ts > ct else (self._ts[ci - 1], ct)
        # Check if the interpolation is possible
        if ts - tl > 2 * self._interval or tr - ts > 2 * self._interval:
            if not force_interpolation:
                raise InterpolationError()
        alpha = (ts - tl) / (tr - tl)
        return Transform.interpolate(self._p[tl], self._p[tr], alpha)

    def between(self, ts1, ts2, force_interpolation=False):
        """
        Get the transform between two specific timestamps (interpolated if
        needed, also see documentation for at()). If any of the requested
        timestamps is "outside" of the trajectory, then ExtrapolationError
        is raised.

        Parameters
        ----------
        ts1 : double
            The first timestamp.
        ts2 : double
            The second timestamp.
        force_interpolation: boolean
            Force interpolation even if the time difference with the nearest
            poses is too large.
        """
        t1 = self.at(ts1, force_interpolation)
        t2 = self.at(ts2, force_interpolation)
        return t1.transform_to(t2)

    def distances_along(self):
        """
        Get a list of distances along the trajectory, where each item
        corresponds to the total distance travelled from the beginning of the
        trajectory up to the corresponding frame.
        """
        distances = [0]
        sum = 0
        for i in range(len(self._p) - 1):
            t = self.between(self._ts[i + 1], self._ts[i])
            sum += t.distance()
            distances.append(sum)
        return distances

    #def rotations_along(self, scale):
        #keys = sorted(self.keys())
        #transforms = [self[keys[i + 1]].transform_to(self[keys[i]])
                      #for i in range(len(keys) - 1)]
        #rotations = [0]
        #sum = 0
        #for t in transforms:
            #sum += t.angle() * scale
            #rotations.append(sum)
        #return rotations

    #def median_interval(self):
        #stamps = self.timestamps()
        #return np.median([s - t for s, t in zip(stamps[1:], stamps[:-1])])

    def find_closest_timestamp(self, ts):
        """
        Find the timestamp in the trajectory that is closest to the given one.

        Parameters
        ----------
        ts : double
            The timestamp.

        Returns
        -------
        (timestamp, index): tuple(double, int)
            The closest timestamp and its index in the list of timestamps.
        """
        timestamps = self.timestamps
        beginning = 0
        difference = abs(timestamps[0] - ts)
        best = 0
        end = len(timestamps)
        while beginning < end:
            middle = int((end + beginning) / 2)
            if abs(timestamps[middle] - ts) < difference:
                difference = abs(timestamps[middle] - ts)
                best = middle
            if ts == timestamps[middle]:
                return ts, middle
            elif timestamps[middle] > ts:
                end = middle
            else:
                beginning = middle + 1
        return timestamps[best], best

    #def number_of_failures(self):
        #return sum(self._f.values())

    def __len__(self):
        return len(self._ts)

    def transform(self, transform):
        """
        Apply the given transform to each pose in the trajectory.

        Parameters
        ----------
        transform : Transform
            The transfrom to be applied to the trajectory.
        """
        for ts in self._ts:
            self._p[ts] = Transform(np.dot(transform.m, self._p[ts].m))


    def points(self, interval=None):
        """
        Get an array with all points of the trajectory in the chronological
        order, optionally filtered based on the timestamp.

        Parameters
        ----------
        interval : tuple(float, float)
            If supplied, then only the points with timestamps greater than
            interval[0] and lesser than interval[1] are returned.

        Returns
        -------
        res : narray
            Each row has three floats: x, y, and z coordinates of a point.
        """
        if interval is not None:
            p = [(self._p[k].x, self._p[k].y, self._p[k].z) for k in self._ts
                 if k > interval[0] and k < interval[1]]
        else:
            p = [(self._p[k].x, self._p[k].y, self._p[k].z) for k in self._ts]
        return np.array(p)


def load(file):
    poses = dict()
    failures = dict()
    for line in file:
        line = line.replace(',', ' ').replace('\t', ' ')
        if not line or line[0] == '#':
            continue
        tokens = [v.strip() for v in line.split(' ')]
        n = [float(t) for t in tokens if t != '' and t != '#']
        if len(n) != 8 or n[4:8] == [0, 0, 0, 0]:
            continue
        if any(np.isnan(v) for v in n):
            sys.stderr.write('Warning: line has NaNs, skipping line.\n')
            continue
        n[4], n[5], n[6], n[7] = n[7], n[4], n[5], n[6]
        poses[n[0]] = Transform.from_translation_and_rotation(n[1:4], n[4:8])
        failures[n[0]] = '#' in line
    return Trajectory(poses, failures)


def save(file, trajectory, title='', bagfile=''):
    file.write('# %s\n' % title)
    file.write('# file: "%s"\n' % bagfile)
    file.write('# timestamp tx ty tz qx qy qz qw\n')
    for ts in trajectory.timestamps:
        transform = trajectory.at(ts)
        t, q = transform.translation, transform.quaternion
        file.write('%.4f %.4f %.4f %.4f %.4f %.4f %.4f %.4f\n'
                   % (ts, t[0], t[1], t[2], q[3], q[0], q[1], q[2]))
