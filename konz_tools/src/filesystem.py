import os
import os.path


class Folder:

    def __init__(self, path, subfolders=None):
        self._path = path
        if not os.path.isdir(path):
            os.makedirs(path)
        self._subfolders = dict()
        if isinstance(subfolders, list):
            for f in subfolders:
                p = os.path.join(path, f)
                if not os.path.isdir(p):
                    os.mkdir(p)
                self._subfolders[f] = p

    def get_path(self):
        return self._path

    def list(self):
        for d in os.listdir(self._path):
            yield d

    def contains(self, filename):
        return os.path.exists(self.__call__(filename))

    def __getattr__(self, name):
        if name in self._subfolders:
            return self._subfolders[name]

    def __call__(self, filename):
        return os.path.join(self._path, filename)
