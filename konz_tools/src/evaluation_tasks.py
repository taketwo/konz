from os.path import join
from subprocess import check_output, call, CalledProcessError


class EvaluationTask:

    def __init__(self, method):
        self._method = method
        self._pname = 'konz_%s' % self._method
        try:
            result = check_output(['rospack', 'find', self._pname])
            self._pfolder = result.rstrip()
        except CalledProcessError:
            raise Exception('"%s" is not a valid method.' % method)
        self._opts = {'config': join(self._pfolder, 'config', 'default.yaml'),
                      'bag': None}
        self._topics = None

    @property
    def package_name(self):
        return self._pname

    @property
    def package_folder(self):
        return self._pfolder

    def set_config(self, value, relative=False):
        if relative:
            self._opts['config'] = join(self._pfolder, 'config', value)
        else:
            self._opts['config'] = value

    def set_output(self, value):
        self._opts['output'] = value

    def set_limit(self, value):
        self._opts['limit'] = value

    def set_bag(self, bag):
        self._opts['bag'] = bag.abspath
        if self._topics is None:
            self._topics = bag.dataset.topics

    def set_topics(self, value):
        self._topics = value

    def run(self):
        cmd = ['rosrun', self._pname, self._exec_name]
        for opt, val in self._opts.viewitems():
            cmd.append('--%s' % opt)
            if not isinstance(val, bool):
                cmd.append(val)
        call(cmd + self._topics)

    def __str__(self):
        out = '%s [ %s ]\n' % (self._title, self._method)
        out += 'Options:\n'
        for opt, val in self._opts.viewitems():
            out += '  %s = %s\n' % (opt, val)
        out += 'Topics: %s' % ' '.join(self._topics)
        return out


class TransformComputerEvaluationTask(EvaluationTask):

    def __init__(self, method):
        EvaluationTask.__init__(self, method)
        self._opts['intervals'] = 'intervals.txt'
        self._opts['output'] = 'transforms.txt'
        self._exec_name = 'transform_computer_evaluation'
        self._title = 'Transform computer evaluation'

    def set_intervals(self, value):
        self._opts['intervals'] = value


class OdometryEvaluationTask(EvaluationTask):

    def __init__(self, method):
        EvaluationTask.__init__(self, method)
        self._opts['output'] = 'trajectory.txt'
        self._exec_name = 'odometry_evaluation'
        self._title = 'Odometry evaluation'

    def set_camera_orientation(self, wxyz):
        self._opts['camera-orientation'] = ' '.join([str(a) for a in wxyz])

    def set_planar(self, planar):
        self._opts['planar'] = planar
