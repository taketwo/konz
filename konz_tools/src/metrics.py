import sys
import numpy as np


def drift_per_second(est, gt, delta=1, delta_discrepancy=0.05):
    """
    Calculates drift per second for each pose in the estimated trajectory.

    Parameters
    ----------
    est : Trajectory
        Estimated trajectory.
    gt : Trajectory
        Ground truth trajectory.
    delta : float
        Compute drift per 'delta' seconds instead.
    delta_discrepancy : float
        Accept pose pair if the time difference belongs to the interval
        [delta * (1 - delta_discrepancy) ; delta * (1 + delta_discrepancy)].

    Returns
    -------
    res : narray
        Each row has three floats: timestamp from estimated trajectory,
        translational error (m), rotational error (rad).
    """
    errors = []
    for ts1 in est.timestamps:
        ts2, j = est.find_closest_timestamp(ts1 + delta)
        if abs(ts1 + delta - ts2) > delta * delta_discrepancy:
            continue  # time difference deviates from delta too much
        if j == len(est) - 1:
            continue  # do not use the last pose
        try:
            estimated = est.between(ts2, ts1)
            groundtruth = gt.between(ts2, ts1)
        except:
            continue  # extrapolation error in groundtruth trajectory
        error = estimated.transform_to(groundtruth)
        errors.append((ts1, error.distance(), error.angle()))
    return np.array(errors)


def drift_per_frame(est, gt, delta=1):
    """
    Calculates drift per frame for each pose in the estimated trajectory.

    Parameters
    ----------
    est : Trajectory
        Estimated trajectory.
    gt : Trajectory
        Ground truth trajectory.
    delta : float
        Compute drift per 'delta' frames instead.

    Returns
    -------
    res : narray
        Each row has three floats: timestamp from estimated trajectory,
        translational error (m), rotational error (rad).
    """
    errors = []
    for ts1, ts2 in zip(est.timestamps[:-1], est.timestamps[1:]):
        try:
            estimated = est.between(ts2, ts1)
            groundtruth = gt.between(ts2, ts1)
        except:
            continue  # extrapolation error in groundtruth trajectory
        error = estimated.transform_to(groundtruth)
        errors.append((ts1, error.distance(), error.angle()))
    return np.array(errors)


def rmse(data):
    """
    Compute RMSE (root-mean-squared error) for the given data.
    """
    return np.sqrt(np.dot(data, data) / len(data))


def print_stats(title, data, unit='', out=sys.__stdout__):
    """
    Print statistics for the given data array.
    """
    print >> out, title
    print >> out, "# RMSE    %f %s" % (rmse(data), unit)
    print >> out, "# Mean    %f %s" % (np.mean(data), unit)
    print >> out, "# Median  %f %s" % (np.median(data), unit)
    print >> out, "# Std     %f %s" % (np.std(data), unit)
    print >> out, "# Min     %f %s" % (np.min(data), unit)
    print >> out, "# Max     %f %s" % (np.max(data), unit)
    print >> out, "# Items   %i" % len(data)


def _find_closest_index(L, t):
    """
    Find the index of the item in the list with the value closest to the query.

    Parameters
    ----------
    L : list
        List with values to be searched.
    t : type of values in L
        Query value.
    """
    beginning = 0
    difference = abs(L[0] - t)
    best = 0
    end = len(L)
    while beginning < end:
        middle = int((end + beginning) / 2)
        if abs(L[middle] - t) < difference:
            difference = abs(L[middle] - t)
            best = middle
        if t == L[middle]:
            return middle
        elif L[middle] > t:
            end = middle
        else:
            beginning = middle + 1
    return best
