import os
import os.path
import re
import yaml

from filesystem import Folder


class Dataset:

    def __init__(self, name):
        path = os.path.join(os.environ['KONZ_DATA'], name)
        self.name = name
        self.folders = Folder(path, ['groundtruth', 'intervals'])
        try:
            desc = yaml.load(open(path + '.yaml', 'r'))
            if 'topics' in desc:
                self.topics = desc['topics']
            if 'calibration' in desc:
                self.calibration = desc['calibration']
            if 'frames' in desc:
                self.frames = desc['frames']
            if 'pose' in desc:
                self.pose = desc['pose']
        except Exception as e:
            pass

    def get_bags(self):
        from bag import Bag
        folder = self.folders.get_path()
        files = [os.path.join(folder, item) for item in os.listdir(folder)]
        return [Bag(file) for file in files if Bag.is_bag(file)]

    def get_bag_names(self):
        from bag import Bag
        bag_names = list()
        folder = self.folders.get_path()
        for file in os.listdir(folder):
            if Bag.is_bag(os.path.join(folder, file)):
                bag_names.append(os.path.splitext(file)[0])
        return bag_names

    @classmethod
    def which_contains(cls, path):
        regex = os.environ['KONZ_DATA'] + r'/(\w+)'
        try:
            name = re.match(regex, os.path.abspath(path)).groups()[0]
            return Dataset(name)
        except AttributeError:
            raise Exception("Unable to recognize the dataset to which path "
                            "'%s' belongs." % path)
