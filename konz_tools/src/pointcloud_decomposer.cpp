#include <ros/ros.h>
#include <rosbag/bag.h>
#include <rosbag/view.h>
#include <cv_bridge/cv_bridge.h>
#include <opencv/cv.h>
#include <pcl/point_cloud.h>
#include <pcl/point_types.h>
#include <pcl_ros/point_cloud.h>
#include <sensor_msgs/PointCloud2.h>
#include <sensor_msgs/Image.h>
#include <sensor_msgs/CameraInfo.h>
#include <sensor_msgs/image_encodings.h>

int main(int argc, const char** argv)
{
  if (argc != 6)
  {
    ROS_ERROR("Usage: %s <bag> <cloud topic> <color topic> <depth topic> <camera info topic>", argv[0]);
    return 1;
  }

  std::string bag(argv[1]);
  std::string color_topic(argv[3]);
  std::string depth_topic(argv[4]);
  std::string camera_info_topic(argv[5]);

  size_t width = 160;
  size_t height = 120;
  float fx = 131.25;
  float cx = 79.5;
  float fy = 131.25;
  float cy = 59.5;
  sensor_msgs::CameraInfoPtr camera_info = boost::make_shared<sensor_msgs::CameraInfo>();
  camera_info->width = width;
  camera_info->height = height;
  camera_info->distortion_model = "plumb_bob";
  camera_info->D = {0, 0, 0, 0, 0};
  camera_info->K = boost::array<double, 9>{{fx, 0, cx, 0, fy, cy, 0, 0, 1}};
  camera_info->R = boost::array<double, 9>{{1, 0, 0, 0, 1, 0, 0, 0, 1}};
  camera_info->P = boost::array<double, 12>{{fx, 0, cx, 0, 0, fy, cy, 0, 0, 0, 1, 0}};

  rosbag::Bag bag_in(bag, rosbag::bagmode::Read);
  rosbag::Bag bag_out(bag + ".out", rosbag::bagmode::Write);
  rosbag::View view(bag_in, rosbag::TopicQuery(std::vector<std::string>{argv[2]}));

  for (const auto& m : view)
  {
    sensor_msgs::PointCloud2ConstPtr msg = m.instantiate<sensor_msgs::PointCloud2>();
    if (msg->width != width || msg->height != height)
    {
      ROS_ERROR("Invalid point cloud size, only 160x120 is supported.");
      return 1;
    }

    // Build color image
    sensor_msgs::ImagePtr color = boost::make_shared<sensor_msgs::Image>();
    try
    {
      pcl::toROSMsg(*msg, *color);
      color->header = msg->header;
    }
    catch (std::runtime_error& e)
    {
      ROS_ERROR("Error in converting point cloud to image message: %s.", e.what());
      return 1;
    }

    // Build depth
    sensor_msgs::ImagePtr depth = boost::make_shared<sensor_msgs::Image>();
    depth->header = msg->header;
    depth->width = width;
    depth->height = height;
    depth->encoding = sensor_msgs::image_encodings::TYPE_32FC1;
    depth->step = width * sizeof(float);
    depth->data.resize(depth->step * height);
    float* data = reinterpret_cast<float*>(&depth->data[0]);

    // First set all points to NANs
    for (size_t i = 0; i < width * height; i++)
      data[i] = std::numeric_limits<float>::quiet_NaN();
    // Convert pointcloud to PCL format
    pcl::PointCloud<pcl::PointXYZ> cloud;
    pcl::fromROSMsg(*msg, cloud);
    // Go through all point and project back
    for (size_t i = 0; i < cloud.points.size(); ++i)
    {
      if (!isnan(cloud.points[i].x))
      {
        float z = cloud.points[i].z;
        int x = cloud.points[i].x * fx / z + cx;
        int y = cloud.points[i].y * fy / z + cy;
        ROS_ASSERT_MSG((x >= 0 && x < static_cast<int>(width)), "X index out of bounds when re-projecting a point.");
        ROS_ASSERT_MSG((y >= 0 && y < static_cast<int>(height)), "Y index out of bounds when re-projecting a point.");
        data[y * width + x] = z;
      }
    }

    // Update camera info
    camera_info->header = msg->header;

    // Write messages to bag file
    bag_out.write(color_topic, color->header.stamp, *color);
    bag_out.write(depth_topic, depth->header.stamp, *depth);
    bag_out.write(camera_info_topic, camera_info->header.stamp, *camera_info);
  }

  bag_in.close();
  bag_out.close();
  return 0;
}

