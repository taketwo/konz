#!/usr/bin/env python

from __future__ import division

import os
import numpy as np
import argparse
import subprocess
import matplotlib.pyplot as plt
from mpl_toolkits.axes_grid1 import ImageGrid
from matplotlib import rc
rc('text', usetex=True)
rc('font', family='serif', size=8)

PACKAGE = 'konz_tools'

import roslib
roslib.load_manifest(PACKAGE)

METHODS = ['dvo', 'fovis', 'pm']
LATEX = dict(zip(METHODS, ['\\texttt{%s}' % m.upper() for m in METHODS]))

SEQUENCES = ['fr1/xyz',
             'fr1/rpy',
             'fr1/room',
             'fr1/floor',
             'fr1/desk',
             'fr1/desk2',
             'fr2/xyz',
             'fr2/rpy',
             'fr2/desk',
             'fr3/long_office_household']


class TCData:

    def __init__(self, name, file, discard_failed=True, upper_limit=None):
        self.name = name
        data = np.loadtxt(file)
        # successes/failures
        self.failed = data[:, -1].astype(np.bool)
        self.succeeded = np.logical_not(self.failed)
        # filter out failder transforms
        if discard_failed:
            data = data[self.succeeded, :]
        # filter out too large transforms
        if upper_limit is not None:
            not_too_large = np.logical_and(data[:, 0] <= upper_limit[0],
                                           data[:, 1] <= upper_limit[1])
            data = data[not_too_large, :]
        # filter out too small transforms (to avoid divizion by zero)
        data = data[np.logical_and(data[:, 0] > 0.0001, data[:, 1] > 0.0001)]
        # ground truth distance and angle
        self.gtd = data[:, 0]
        self.gta = data[:, 1]
        # estimated distance and angle
        self.etd = data[:, 2]
        self.eta = data[:, 3]
        # difference in distance and angle
        self.dd = data[:, 4]
        self.da = data[:, 5]
        # error distance and angle
        self.ed = data[:, 6]
        self.ea = data[:, 7]
        # translation direction error
        self.tde = data[:, 8]
        # runtime
        self.rt = data[:, 9]
        # relative differences and errors
        self.ddp = self.dd / self.gtd
        self.dap = self.da / self.gta
        self.edp = self.ed / self.gtd
        self.eap = self.ea / self.gta


def plot_2d_histograms(D, fT, fX, fY,
                       bins=(0.01, 0.005),
                       ticks=(0.05, 0.05),
                       extent=None,
                       clamp=0.1,
                       func=np.mean):
    """
    D - TCData
    fX - name of the field in D that should be used for x axis
    fY - name of the field in D that should be used for y axis
    fT - name of the field in D that should be used as the value data
    """
    fig = plt.figure()
    grid = ImageGrid(fig, 111,
                     nrows_ncols=(1, len(D)),
                     direction='row',
                     axes_pad=0.2,
                     label_mode='1',
                     add_all=True,
                     share_all=False,
                     cbar_location='right',
                     cbar_mode='single',
                     cbar_size=0.2,
                     cbar_pad=0.2)
    ext = extent or [0, 0.15, 0, 0.3]
    tX = np.arange(ticks[0], extent[1] + 0.01, ticks[0])
    tY = np.arange(extent[2], extent[3] + 0.01, ticks[1])
    bX = np.arange(extent[0], extent[1], bins[0])
    bY = np.arange(extent[2], extent[3], bins[1])
    for d, a in zip(D, grid):
        X = d.__dict__[fX]
        Y = d.__dict__[fY]
        T = d.__dict__[fT]
        h = np.empty([len(bY) - 1, len(bX) - 1])
        x_indices = [np.logical_and(X >= bX[x], X < bX[x + 1])
                     for x in range(len(bX) - 1)]
        y_indices = [np.logical_and(Y >= bY[y], Y < bY[y + 1])
                     for y in range(len(bY) - 1)]
        for x in range(len(bX) - 1):
            for y in range(len(bY) - 1):
                items = T[np.logical_and(x_indices[x], y_indices[y])]
                value = func(items)
                if clamp is not None:
                    value = value if value < clamp else clamp
                h[-y-1, x] = value
        i = a.imshow(h, extent=ext, interpolation='nearest', cmap=plt.cm.jet)
        a.set_title(d.name)
    a.cax.colorbar(i)
    a.cax.toggle_label(True)
    grid[0].set_xticks(tX)
    grid[1].set_xticks(tX)
    grid[1].xaxis.set_ticks_position('bottom')
    grid[2].set_xticks(tX)
    grid[2].xaxis.set_ticks_position('bottom')
    grid[0].set_yticks(tY)
    return fig, grid


def save_pdf(fig, name, size=None, bg='white'):
    if size:
        fig.set_figwidth(size[0])
        fig.set_figheight(size[1])
    fig.savefig(name + '.pdf', dpi=300, format='pdf', facecolor=bg)


if __name__ == '__main__':
    parser = argparse.ArgumentParser(description='''
    This script creates a bunch of TC error histograms that present the
    relations between estimation errors and the magnitudes of transforms.
    ''', formatter_class=argparse.RawDescriptionHelpFormatter)
    parser.add_argument('--bg', default='white', help='background color')
    args = parser.parse_args()

    data = list()
    for m in METHODS:
        f = os.path.join(os.environ['KONZ_EXPERIMENTS'], 'tc', m)
        aggregate = '%s/errors' % f
        os.remove(aggregate)
        for s in SEQUENCES:
            t = s.split('/')
            fn = '%s/rgbd_dataset_freiburg%s_%s.errors' % (f, t[0][2], t[1])
            subprocess.call(['cat', fn], stdout=open(aggregate, 'a'))
        count = subprocess.check_output(['wc', '-l', aggregate])
        print '%s: %s frame pairs' % (m, count.split(' ')[0])
        data.append(TCData(LATEX[m], aggregate, upper_limit=(0.3, 0.15)))

    fig, s = plot_2d_histograms(data, 'ed', 'gta', 'gtd',
                                bins=(0.01, 0.01),
                                clamp=0.1,
                                extent=[0, 0.15, 0, 0.3])

    save_pdf(fig, 'translational-error-vs-groundtruth-transform',
             size=(4.8, 2.5), bg=args.bg)

    fig, s = plot_2d_histograms(data, 'ea', 'gta', 'gtd',
                                bins=(0.01, 0.01),
                                clamp=0.05,
                                extent=[0, 0.15, 0, 0.3])

    save_pdf(fig, 'rotational-error-vs-groundtruth-transform',
             size=(4.8, 2.5), bg=args.bg)

    data = list()
    for m in METHODS:
        f = os.path.join(os.environ['KONZ_EXPERIMENTS'], 'tc', m)
        aggregate = '%s/errors' % f
        data.append(TCData(LATEX[m], aggregate, upper_limit=(0.15, 0.15)))

    fig, s = plot_2d_histograms(data, 'edp', 'eta', 'etd',
                                clamp=0.3,
                                extent=[0, 0.15, 0, 0.15],
                                ticks=(0.05, 0.03),
                                bins=(0.01, 0.01))

    save_pdf(fig, 'relative-translational-error-vs-estimated-transform',
             size=(4.8, 1.5), bg=args.bg)

    fig, s = plot_2d_histograms(data, 'eap', 'eta', 'etd',
                                clamp=0.3,
                                extent=[0, 0.15, 0, 0.15],
                                ticks=(0.05, 0.03),
                                bins=(0.01, 0.01))

    save_pdf(fig, 'relative-rotational-error-vs-estimated-transform',
             size=(4.8, 1.5), bg=args.bg)
