#!/usr/bin/env python

import os
import sys
import math
import numpy as np
import argparse

PACKAGE = 'konz_tools'

import roslib
roslib.load_manifest(PACKAGE)

from metrics import rmse

METHODS = ['dvo', 'fovis', 'pm']

SEQUENCES = [('Testing and Debugging',
             [('fr1/xyz', 0.244, 8.920),
              ('fr1/rpy', 0.062, 50.147),
              ('fr2/xyz', 0.058, 1.716),
              ('fr2/rpy', 0.014, 5.774)]),
             ('Handheld SLAM',
             [('fr1/360', 0.210, 41.600),
              ('fr1/floor', 0.258, 15.071),
              ('fr1/desk', 0.413, 23.327),
              ('fr1/desk2', 0.426, 29.308),
              ('fr1/room', 0.334, 29.882),
              ('fr2/360_hemisphere', 0.163, 20.569),
              #('fr2/360_kidnap', 0.304, 13.425),
              ('fr2/desk', 0.193, 6.338),
              #('fr2/large_no_loop', 0.243, 15.090),
              #('fr2/large_with_loop', 0.231, 17.211),
              ('fr3/long_office_household', 0.249, 10.188)]),
             ('Robot SLAM',
             [('fr2/pioneer_360', 0.225, 12.053),
              ('fr2/pioneer_slam', 0.261, 13.379),
              ('fr2/pioneer_slam2', 0.190, 12.209),
              ('fr2/pioneer_slam3', 0.164, 12.339)]),
             ('Structure vs. Texture',
             [('fr3/nostructure_notexture_far', 0.196, 2.712),
              ('fr3/nostructure_notexture_near_withloop', 0.319, 11.241),
              ('fr3/nostructure_texture_far', 0.299, 2.890),
              ('fr3/nostructure_texture_near_withloop', 0.242, 7.430),
              ('fr3/structure_notexture_far', 0.166, 4.000),
              ('fr3/structure_notexture_near', 0.109, 6.247),
              ('fr3/structure_texture_far', 0.193, 4.323),
              ('fr3/structure_texture_near', 0.141, 7.677)]),
             ('Dynamic objects',
             [('fr2/desk_with_person', 0.121, 5.340),
              ('fr3/sitting_static', 0.011, 1.699),
              ('fr3/sitting_xyz', 0.132, 3.562),
              ('fr3/sitting_halfsphere', 0.180, 19.094),
              ('fr3/sitting_rpy', 0.042, 23.841),
              ('fr3/walking_static', 0.012, 1.388),
              ('fr3/walking_xyz', 0.208, 5.490),
              ('fr3/walking_halfsphere', 0.221, 18.267),
              ('fr3/walking_rpy', 0.091, 20.903)])]


class Table:

    HEADER = """\\begin{table}
  \\centering
  \\caption
  {
     %s
  }
  \\label{%s}
  \\begin{tabular}{@{}%s@{}}
    \\toprule
"""

    FOOTER = """    \\bottomrule
  \\end{tabular}
\\end{table}
"""

    PHANTOM = '\\phantom{abc}'

    def __init__(self, caption, label, columns):
        self.columns = columns
        self.code = self.HEADER % (caption, label, columns)
        self.mc = None

    def header(self, desc):
        items = list()
        self.mc = list()
        i = 1
        for d in desc:
            if isinstance(d, tuple) and len(d) == 3:
                items.append('\\multicolumn{%i}{%s}{%s}' % d)
                self.mc.append((i, i + d[0] - 1))
                i += d[0]
            else:
                items.append(d)
                i += 1
        line = ' & '.join(items)
        self.code += '    %s \\\\\n' % line

    def midrule(self, rules=None):
        if rules is None or (rules == 'auto' and self.mc is None):
            self.code += '    \\midrule\n'
            return
        if rules == 'auto':
            rules, self.mc = self.mc, None
        line = ' '.join(['\\cmidrule{%i-%i}' % r for r in rules])
        self.code += '    %s\n' % line

    def row(self, items):
        if len(items) < len(self.columns):
            stuffer = [''] * (len(self.columns) - len(items))
            items[len(items):len(self.columns)] = stuffer
        line = ' & '.join(items).replace('_', '\_')
        self.code += '    %s \\\\\n' % line

    def write(self, out=sys.__stdout__):
        out.write(self.code + self.FOOTER)


def to_str(l, format='%.3f', emphasize=None):
    nl = [format % i for i in l]
    if emphasize:
        val = format % emphasize(l)
        for i, v in enumerate(nl):
            if v == val:
                nl[i] = '\\textbf{%s}' % nl[i]
    return nl


if __name__ == '__main__':
    parser = argparse.ArgumentParser(description='''
    This script creates a latex code for a table that presents the
    translational and rotational drifts per second for different sequences and
    odometry systems.
    ''', formatter_class=argparse.RawDescriptionHelpFormatter)
    parser.add_argument('output', nargs='?',
                        help='save latex code to a file')
    args = parser.parse_args()

    CAPTION = ('Evaluation results for the three visual odometry systems on '
               'the sequences from \RGBD dataset. The second group column '
               'shows the average translational (\mps) and rotational (\dps) '
               'velocities in a sequence according to the ground truth data. '
               'The third and fourth group columns show RMSE of translational '
               'drift (\mps) and rotational drift (\dps) respectively for '
               'each odometry system.')
    table = Table(CAPTION, 'tab:drifts', 'lccccccccccc')
    table.header(['',
                  Table.PHANTOM,
                  (2, 'c', 'Avg. velocity'),
                  Table.PHANTOM,
                  (3, 'c', 'Translational drift'),
                  Table.PHANTOM,
                  (3, 'c', 'Rotational drift')])
    table.midrule('auto')
    table.header(['Sequence name',
                  '',
                  'trans', 'rot',
                  '',
                  '\\' + METHODS[0], '\\' + METHODS[1], '\\' + METHODS[2],
                  '',
                  '\\' + METHODS[0], '\\' + METHODS[1], '\\' + METHODS[2]])

    folder = os.environ['KONZ_EXPERIMENTS'] + '/odom'
    TRFMT = '%.3f'
    ROTFMT = '%.2f'
    for group, sequences in SEQUENCES:
        table.midrule()
        table.row(['%-30s' % group])
        table.midrule()
        for seq, tr, rot in sequences:
            d, b = tuple(seq.split('/'))
            bag = 'rgbd_dataset_freiburg%s_%s' % (d[2], b)
            t_rmse = list()
            r_rmse = list()
            for m in METHODS:
                data = np.loadtxt(os.path.join(folder, m, bag) + '.dps')
                t_rmse.append(rmse(data[:, 1]))
                r_rmse.append(math.degrees(rmse(data[:, 2])))
            sshort = seq.replace('ure', '').replace('_withloop', '')
            table.row(['%s' % sshort, ''] + [TRFMT % tr, ROTFMT % rot, ''] +
                      to_str(t_rmse, format=TRFMT, emphasize=min) +
                      [''] + to_str(r_rmse, format=ROTFMT, emphasize=min))

    if args.output:
        table.write(open(args.output, 'w'))
    else:
        table.write()
