#!/usr/bin/env python

from __future__ import division
import os
import argparse
from os.path import join
import matplotlib.pyplot as plt

PACKAGE = 'konz_tools'

import roslib
roslib.load_manifest(PACKAGE)

METHODS = ['dvo', 'fovis', 'pm']
LATEX = ['\\texttt{%s}' % m.upper() for m in METHODS]

SEQUENCES = ['brsu/long2']


import trajectory
from filesystem import Folder
from bag import Bag
from plot_2d_trajectory import TrajectoryPlot


class TrajectoriesPlot(TrajectoryPlot):

    STYLES = ['-g', '-r', '-b', '-k']
    LINEWIDTH = 1

    def __init__(self, subplots):
        self.fig, self.axes = plt.subplots(1, len(subplots), sharey=True)
        self.xlim = None
        self.xlim = (0, 1)
        self.ylim = None
        self.ylim = (0, 1)
        self.padding = 0.2
        for a, s in zip(self.axes, subplots):
            a.set_title(s)
            self.ax = a
            self.make_grid(1.0, 0.5)

    def add_trajectory(self, trajectory, style,  subplot=None):
        points = trajectory.points()
        if subplot is None:
            for a in self.axes:
                a.plot(points[:, 0], points[:, 1], style[0], lw=style[1])
        else:
            self.axes[subplot].plot(points[:, 0], points[:, 1], style[0],
                                    linewidth=style[1])
        tmax = points.max(axis=0)
        tmin = points.min(axis=0)
        self.update_limits((tmin[0], tmax[0]), (tmin[1], tmax[1]))
        for a in self.axes:
            a.set_xlim(*self.xlim)
            a.set_ylim(*self.ylim)

    def save_pdf(self, name, width=4.8, height=None, bg='white'):
        xwidth = (self.xlim[1] - self.xlim[0]) * len(self.axes)
        ywidth = self.ylim[1] - self.ylim[0]
        ratio = ywidth / xwidth
        height = height or width * ratio - 0.4
        #self.fig.tight_layout()
        self.fig.set_figwidth(width)
        self.fig.set_figheight(height)
        self.fig.savefig(name + '.pdf', dpi=300, format='pdf', facecolor=bg)


if __name__ == '__main__':
    parser = argparse.ArgumentParser(description='''
    This script creates a bunch 2d trajectory plots for selected sequences in
    BRSU dataset.
    ''', formatter_class=argparse.RawDescriptionHelpFormatter)
    args = parser.parse_args()

    plot = TrajectoriesPlot(LATEX)
    for s in SEQUENCES:
        bag = Bag(os.environ['KONZ_DATA'] + '/' + s + '.bag')
        gt = bag.load_groundtruth_trajectory()
        plot.add_trajectory(gt, style=('-g', 0.4))
        for i, m in enumerate(METHODS):
            f = Folder(join(os.environ['KONZ_EXPERIMENTS'], 'odom', m,
                            'planar'))
            et = trajectory.load(open(f(bag.stem + '.trajectory'), 'r'))
            plot.add_trajectory(et, style=('-r', 1.2), subplot=i)
    plot.save_pdf('%s-planar-odometry' % bag.stem)
