#!/usr/bin/env python

from __future__ import division
import os
import numpy as np
import argparse
import math
import subprocess
from os.path import join

PACKAGE = 'konz_tools'

import roslib
roslib.load_manifest(PACKAGE)

METHODS = ['pm', 'fovis', 'dvo']
LATEX = ['\\texttt{%s}' % m.upper() for m in METHODS]

SEQUENCES = [('Testing and Debugging',
             ['fr1/xyz',
              'fr1/rpy',
              'fr2/xyz',
              'fr2/rpy']),
             ('Handheld SLAM',
             ['fr1/360',
              'fr1/floor',
              'fr1/desk',
              'fr1/desk2',
              'fr1/room',
              #'fr2/360_hemisphere',
              #'fr2/360_kidnap',
              'fr2/desk',
              #'fr2/large_no_loop',
              #'fr2/large_with_loop',
              'fr3/long_office_household']),
             ('Robot SLAM',
             ['fr2/pioneer_360',
              'fr2/pioneer_slam',
              'fr2/pioneer_slam2',
              'fr2/pioneer_slam3']),
             ('Structure vs. Texture',
             ['fr3/nostructure_notexture_far',
              'fr3/nostructure_notexture_near_withloop',
              'fr3/nostructure_texture_far',
              'fr3/nostructure_texture_near_withloop',
              'fr3/structure_notexture_far',
              'fr3/structure_notexture_near',
              'fr3/structure_texture_far',
              'fr3/structure_texture_near']),
             ('Dynamic objects',
             ['fr2/desk_with_person',
              'fr3/sitting_static',
              'fr3/sitting_xyz',
              'fr3/sitting_halfsphere',
              'fr3/sitting_rpy',
              'fr3/walking_static',
              'fr3/walking_xyz',
              'fr3/walking_halfsphere',
              'fr3/walking_rpy'])]


from filesystem import Folder
from plot_drift_boxplot import DriftBoxplot
from metrics import rmse


if __name__ == '__main__':
    parser = argparse.ArgumentParser(description='''
    This script creates a bunch drift boxplots for selected sequences in FR
    dataset.
    ''', formatter_class=argparse.RawDescriptionHelpFormatter)
    args = parser.parse_args()

    for g, s in SEQUENCES:
        gr = g.lower().replace(' ', '_').replace('.', '')
        trans = list()
        rot = list()
        print 'Group:', g
        for m in METHODS:
            print 'Method:', m
            f = Folder(join(os.environ['KONZ_EXPERIMENTS'], 'odom', m))
            aggregate = f(gr)
            if os.path.exists(aggregate):
                os.remove(aggregate)
            for seq in s:
                t = seq.split('/')
                fn = f('rgbd_dataset_freiburg%s_%s.dps' % (t[0][2], t[1]))
                subprocess.call(['cat', fn], stdout=open(aggregate, 'a'))
            dps = f(aggregate)
            data = np.loadtxt(dps)
            trans.append(data[:, 1])
            rot.append(data[:, 2] * 180.0 / math.pi)
            print 'RMSE traslational:', rmse(data[:, 1])
            print 'RMSE rotational:', rmse(data[:, 2] * 180.0 / math.pi)
        plot = DriftBoxplot(trans, rot, labels=LATEX)
        plot.save_pdf('drifts-boxplot-%s' % gr, width=4.8, height=1.3)
