#!/usr/bin/env python

import argparse

PACKAGE = 'konz_tools'

import roslib
roslib.load_manifest(PACKAGE)

from dataset import Dataset
from create_intervals_file import create_intervals_file


if __name__ == '__main__':
    parser = argparse.ArgumentParser(description='''
    ''', formatter_class=argparse.RawDescriptionHelpFormatter)
    args = parser.parse_args()

    for d in [1, 2, 3]:
        ds = Dataset('fr%i' % d)
        for bag in ds.get_bags():
            'Processing bag', bag.stem
            create_intervals_file(bag, 0.3, 0.15)
