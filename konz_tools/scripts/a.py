from __future__ import division
import matplotlib.pyplot as plt
import matplotlib.pylab as pylab
from mpl_toolkits.axes_grid1 import ImageGrid
from matplotlib import rc
rc('text', usetex=True)
rc('font', family='serif', size=8)
import numpy as np
import sys
import os

PACKAGE = 'konz_tools'

import roslib
roslib.load_manifest(PACKAGE)

from bag import Bag

METHODS = ['pm', 'dvo', 'fovis']


class TCData:

    def __init__(self, name, file,
                 discard_failed=True,
                 upper_limit=None):
        self.name = name
        data = np.loadtxt(file)
        # successes/failures
        self.failed = data[:, -1].astype(np.bool)
        self.succeeded = np.logical_not(self.failed)
        # filter out failder transforms
        if discard_failed:
            data = data[self.succeeded, :]
        # filter out too large transforms
        if upper_limit is not None:
            not_too_large = np.logical_and(data[:, 0] <= upper_limit[0],
                                           data[:, 1] <= upper_limit[1])
            data = data[not_too_large, :]
        # filter out too small transforms (to avoid divizion by zero)
        data = data[np.logical_and(data[:, 0] > 0.0001, data[:, 1] > 0.0001)]
        # ground truth distance and angle
        self.gtd = data[:, 0]
        self.gta = data[:, 1]
        # estimated distance and angle
        self.etd = data[:, 2]
        self.eta = data[:, 3]
        # difference in distance and angle
        self.dd = data[:, 4]
        self.da = data[:, 5]
        # error distance and angle
        self.ed = data[:, 6]
        self.ea = data[:, 7]
        # translation direction error
        self.tde = data[:, 8]
        # runtime
        self.rt = data[:, 9]
        # relative
        self.ddp = self.dd / self.gtd
        self.dap = self.da / self.gta
        self.edp = self.ed / self.gtd
        self.eap = self.ea / self.gta


class OdomData:

    def __init__(self, name, base_folder='./'):
        self.ed = dict()
        self.ea = dict()
        for d in os.listdir(os.path.join(base_folder, name)):
            stem, ext = os.path.splitext(d)
            if ext == '.dps':
                data = np.loadtxt(os.path.join(base_folder, name, d))
                self.ed[stem] = data[:, 1]
                self.ea[stem] = data[:, 2]
        self.name = name

    @property
    def bags(self):
        return self.ed.keys()

    @classmethod
    def load_dataset(cls, folder='./'):
        return [OdomData(n, folder) for n in METHODS]


def load_dataset(limit=None):
    methods = ['pm', 'dvo', 'fovis']
    return [TCData(n, n + '/errors', upper_limit=limit) for n in methods]


def plot_2d_histograms(D, fT, fX, fY,
                       bins=(0.01, 0.005),
                       ticks=(0.05, 0.05),
                       extent=None,
                       clamp=0.1,
                       func=np.mean):
    """
    D - TCData
    fX - name of the field in D that should be used for x axis
    fY - name of the field in D that should be used for y axis
    fT - name of the field in D that should be used as the value data
    """
    fig = plt.figure()
    grid = ImageGrid(fig, 111,
                     nrows_ncols=(1, len(D)),
                     direction='row',
                     axes_pad=0.05,
                     label_mode='1',
                     add_all=True,
                     share_all=False,
                     cbar_location='right',
                     cbar_mode='single',
                     cbar_size='15%',
                     cbar_pad=0.05)
    extent = extent or [0, 0.15, 0, 0.3]
    tX = np.arange(extent[0], extent[1] + 0.01, ticks[0])
    tY = np.arange(ticks[1], extent[3] + 0.01, ticks[1])
    bX = np.arange(extent[0], extent[1], bins[0])
    bY = np.arange(extent[2], extent[3], bins[1])
    for d, a in zip(D, grid):
        X = d.__dict__[fX]
        Y = d.__dict__[fY]
        T = d.__dict__[fT]
        h = np.empty([len(bY) - 1, len(bX) - 1])
        x_indices = [np.logical_and(X >= bX[x], X < bX[x + 1])
                     for x in range(len(bX) - 1)]
        y_indices = [np.logical_and(Y >= bY[y], Y < bY[y + 1])
                     for y in range(len(bY) - 1)]
        for x in range(len(bX) - 1):
            for y in range(len(bY) - 1):
                items = T[np.logical_and(x_indices[x], y_indices[y])]
                value = func(items)
                if clamp is not None:
                    value = value if value < clamp else clamp
                h[-y-1, x] = value
        i = a.imshow(h, extent=extent, interpolation='nearest', cmap=plt.cm.jet)
        a.set_title(d.name)
    a.cax.colorbar(i)
    a.cax.toggle_label(True)
    grid[0].set_xticks(tX)
    grid[1].set_xticks(tX)
    grid[0].set_yticks(tY)
    return fig, grid


# Histogram of first type
#fig, s = plot_2d_histograms(d, 'ed', 'gta', 'gtd',
                            #bins=(0.01, 0.01),
                            #clamp=0.1,
                            #extent=[0, 0.15, 0, 0.3])
# Histogram of second type
#fig, s = plot_2d_histograms(d, 'edp', 'eta', 'etd',
                            #clamp=0.3,
                            #extent=[0, 0.15, 0, 0.15],
                            #bins=(0.01, 0.01))


def plot_box_plots(D, bag, save=False):
    plt.close()
    #fig = plt.figure(figsize=(4.8, 2))
    #grid = ImageGrid(fig, 111,
                     #nrows_ncols=(1, 2),
                     #direction='row',
                     #axes_pad=0.38,
                     #label_mode='1',
                     #add_all=True,
                     #share_all=False)
    fig, grid = plt.subplots(1, 2, sharex=False, sharey=True)
    fig.set_figwidth(4.8)
    fig.set_figheight(2)
    #a1 = fig.add_subplot(111)
    #a2 = a1.twiny()
    #grid = [a1, a2]
    try:
        ed = [d.ed[bag] for d in D]
        ea = [d.ea[bag] for d in D]
    except:
        print 'Missing results for bag %s!' % bag
        return
    names = [d.name for d in D]
    grid[0].boxplot(ed, vert=0, widths=0.65)
    grid[0].yaxis.set_ticklabels(names)
    #grid[0].xaxis.tick_top()
    grid[0].set_title('Translational error (meters)')
    grid[0].grid(True)
    grid[0].set_xscale('log')
    grid[1].boxplot(ea, vert=0, widths=0.65)
    grid[1].yaxis.set_ticklabels(names)
    #grid[1].xaxis.tick_top()
    grid[1].set_title('Rotational error (radians)')
    grid[1].grid(True)
    grid[1].set_xscale('log')
    grid[0].yaxis.tick_right()
    fig.tight_layout()
    if save:
        save_pdf(fig, bag, size=(4.8, 1.3))
    return fig, grid


def save_pdf(fig, name, size=None, bg='white'):
    if size:
        fig.set_figwidth(size[0])
        fig.set_figheight(size[1])
    fig.savefig(name + '.pdf', dpi=300, format='pdf', facecolor=bg)

###############################################################################
#                                 Data statistics                             #
###############################################################################

def print_stats(title, D, unit='', out=sys.__stdout__):
    print >> out, title
    print >> out, "   RMSE    %f %s" % (np.sqrt(np.dot(D, D) / len(D)), unit)
    print >> out, "   Mean    %f %s" % (np.mean(D), unit)
    print >> out, "   Median  %f %s" % (np.median(D), unit)
    print >> out, "   Std     %f %s" % (np.std(D), unit)
    print >> out, "   Min     %f %s" % (np.min(D), unit)
    print >> out, "   Max     %f %s" % (np.max(D), unit)


def is_outlier(points, thresh=3.5):
    """
    Returns a boolean array with True if points are outliers and False
    otherwise.

    Parameters:
    -----------
        points : An numobservations by numdimensions array of observations
        thresh : The modified z-score to use as a threshold. Observations with
            a modified z-score (based on the median absolute deviation) greater
            than this value will be classified as outliers.

    Returns:
    --------
        mask : A numobservations-length boolean array.

    References:
    ----------
        Boris Iglewicz and David Hoaglin (1993), "Volume 16: How to Detect and
        Handle Outliers", The ASQC Basic References in Quality Control:
        Statistical Techniques, Edward F. Mykytka, Ph.D., Editor.
    """
    if len(points.shape) == 1:
        points = points[:, None]
    median = np.median(points, axis=0)
    diff = np.sum((points - median)**2, axis=-1)
    diff = np.sqrt(diff)
    med_abs_deviation = np.median(diff)
    #print 'MAD:', med_abs_deviation

    modified_z_score = 0.6745 * diff / med_abs_deviation

    return modified_z_score > thresh

###########################################################################
#                            Failure detection                            #
###########################################################################
    small_motion = np.logical_and(gtd < 0.01, gta < 0.02)
    okay_motion = np.logical_not(small_motion)
    failures = np.logical_and(np.logical_or(ddp > 0.5, dap > 0.5), okay_motion)
    successes = np.logical_not(failures)
#print 'Missed failures:', np.sum(np.logical_and(failures, success))

###########################################################################
#    (Anything) as a function of translation and rotation [HISTOGRAM]     #
###########################################################################


def tr_histogram(data, X, Y, bins=(0.005, 0.01), empty=1):
    """
    Plot translation-rotation histogram.
    """
    h_bins = [np.arange(0, X.max(), bins[0]), np.arange(0, Y.max(), bins[1])]
    h_base, xe, ye = np.histogram2d(X, Y, bins=h_bins)
    h_data, xe, ye = np.histogram2d(X, Y, bins=h_bins, weights=data)
    h_out = np.empty(h_base.size)
    i = 0
    for d, b in zip(h_data.flatten(), h_base.flatten()):
        if b == 0.0:
            h_out[i] = empty
        else:
            h_out[i] = d / b
        if h_out[i] > 1:
            h_out[i] = 1
        i += 1
    h_out = h_out.reshape(h_base.shape)
    extent = [ye[0], ye[-1], xe[-1], xe[0]]
    plt.close()
    plt.imshow(h_out, extent=extent, interpolation='nearest')
    plt.colorbar()


def bin_view(data, X, Y, bin, bins):
    iX = np.logical_and(X > bin[0][0], X < bin[0][1])
    iY = np.logical_and(Y > bin[1][0], Y < bin[1][1])
    i = np.logical_and(iX, iY)
    d = data[i]
    # Keep only the "good" points
    # "~" operates as a logical not operator on boolean numpy arrays
    filtered = d[~is_outlier(d)]
    #plt.hist(filtered, bins=bins)
    plt.hist(d, bins=bins)
    print 'Median:', np.median(d), np.median(filtered)
    print 'Mean:', np.mean(d), np.mean(filtered)
    print 'Std:', np.std(d), np.std(filtered)


def histogram2d(data, X, Y,
                bins=(0.005, 0.01),
                min=(0, 0),
                clamp=0.5,
                func=np.median):
    """
    Plot 2d histogram.
    """
    bX = np.arange(min[0], X.max(), bins[0])
    bY = np.arange(min[1], Y.max(), bins[1])
    h = np.empty([len(bY) - 1, len(bX) - 1])
    x_indices = [np.logical_and(X >= bX[x], X < bX[x + 1])
                 for x in range(len(bX) - 1)]
    y_indices = [np.logical_and(Y >= bY[y], Y < bY[y + 1])
                 for y in range(len(bY) - 1)]
    for x in range(len(bX) - 1):
        for y in range(len(bY) - 1):
            items = data[np.logical_and(x_indices[x], y_indices[y])]
            value = func(items)
            if clamp is not None:
                value = value if value < clamp else clamp
            h[-y-1, x] = value

    extent = [min[0], X.max(), min[1], Y.max()]
    plt.close()
    plt.imshow(h, extent=extent, interpolation='nearest', cmap=plt.cm.jet)
    plt.colorbar()


def rhistogram2d(data, X, Y,
                 bins=(0.005, 0.01),
                 min=(0, 0),
                 max=None,
                 clamp=0.5,
                 dev=0.1):
    """
    Plot reverse 2d histogram.
    """
    max = max or (X.max(), Y.max())
    bX = np.arange(min[0], max[0], bins[0])
    bY = np.arange(min[1], max[1], bins[1])
    h = np.empty([len(bY) - 1, len(bX) - 1])
    x_indices = [np.logical_and(X >= bX[x], X < bX[x + 1])
                 for x in range(len(bX) - 1)]
    y_indices = [np.logical_and(Y >= bY[y], Y < bY[y + 1])
                 for y in range(len(bY) - 1)]
    for x in range(len(bX) - 1):
        for y in range(len(bY) - 1):
            items = data[np.logical_and(x_indices[x], y_indices[y])]
            #filtered = items[~is_outlier(items)]
            #value = np.std(filtered)
            #value = np.sum(is_outlier(items)) / len(items)
            #value = np.sum(items > dev) / len(items)
            value = np.mean(items)
            if clamp is not None:
                value = value if value < clamp else clamp
            h[-y-1, x] = value

    extent = [min[0], max[0], min[1], max[1]]
    plt.close()
    plt.imshow(h, extent=extent, interpolation='nearest', cmap=plt.cm.jet)
    plt.colorbar()


def chistogram2d(data, X, Y,
                 bins=(0.005, 0.01),
                 min=(0, 0),
                 max=None,
                 clamp=0.5,
                 confidence=0.9):
    """
    Plot confidence 2d histogram.
    """
    max = max or (X.max(), Y.max())
    bX = np.arange(min[0], max[0], bins[0])
    bY = np.arange(min[1], max[1], bins[1])
    h = np.empty([len(bY) - 1, len(bX) - 1])
    x_indices = [np.logical_and(X >= bX[x], X < bX[x + 1])
                 for x in range(len(bX) - 1)]
    y_indices = [np.logical_and(Y >= bY[y], Y < bY[y + 1])
                 for y in range(len(bY) - 1)]
    for x in range(len(bX) - 1):
        for y in range(len(bY) - 1):
            items = data[np.logical_and(x_indices[x], y_indices[y])]
            if len(items):
                value = items.max()
                for dev in np.arange(0.01, 1, 0.005):
                    c = np.sum(items < dev) / len(items)
                    if c > confidence:
                        value = dev
                        break
                if clamp is not None:
                    value = value if value < clamp else clamp
            else:
                value = 0
            h[-y-1, x] = value
    extent = [min[0], max[0], min[1], max[1]]
    plt.close()
    plt.imshow(h, extent=extent, interpolation='nearest', cmap=plt.cm.jet)
    plt.colorbar()


def stat_histogram(data, X, bins=0.002, stat=np.mean):
    # digitize into bins along X, compute statistic over data
    h_bins = np.arange(0, X.max(), bins)
    indices = np.digitize(X, h_bins)
    h_out = np.array([stat(data[indices == i]) for i in range(1, len(h_bins))])
    plt.close()
    plt.scatter(h_bins[:-1], h_out, color='blue')


###############################################################################
#                          Plot a bunch of histograms                         #
###############################################################################

if len(sys.argv) == 2 and sys.argv[1] == 'hist':
    for v in ['ddp', 'dap', 'tde', 'edp', 'eap', 'rt']:
    #for v in ['dd', 'da', 'ed', 'ea']:
        for s, c in zip(['np.mean', 'np.median', 'np.std'],
                        [0.3, 0.3, 0.5, None]):
                        #[0.05, 0.05, 0.05, None]):
            print 'plotting', v, s, 'clamp is', c
            histogram2d(eval(v), gtd, gta, min=(0.003, 0.03),
                        bins=(0.005, 0.01), clamp=c, func=eval(s))
            plt.savefig(v + '-' + s + '.png')


###############################################################################
#                     Superfunctions to plot everything                       #
###############################################################################

def plot_boxplots_for_all_bags(ds):
    d = OdomData.load_dataset()
    for bag in ds.get_bag_names():
        f, a = plot_box_plots(d, bag)
        save_pdf(f, bag, size=(4.8, 1.3))

###########################################################################
#      Translation error as a function of translation [SCATTERPLOT]       #
###########################################################################
#gtd_bins = np.arange(0, gtd.max(), 0.002)
#dd_means = stat_histogram(gtd, dd, gtd_bins)
#ed_means = stat_histogram(gtd, ed, gtd_bins)
#edp_means = stat_histogram(gtd, ed / gtd, gtd_bins)
#ddp_means = stat_histogram(gtd, dd / gtd, gtd_bins, stat=np.median)

#sys.exit()
