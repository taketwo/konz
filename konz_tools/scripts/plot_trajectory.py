#!/usr/bin/env python

import argparse
import numpy as np
import matplotlib as mpl
import matplotlib.pyplot as plt
from mpl_toolkits.mplot3d import Axes3D

PACKAGE = 'konz_tools'

import roslib
roslib.load_manifest(PACKAGE)

import trajectory


if __name__ == '__main__':
    parser = argparse.ArgumentParser(description='''
    Plot trajectory.
    ''')
    parser.add_argument('trajectory', help='trajectory file')
    args = parser.parse_args()

    t = trajectory.load(open(args.trajectory, 'r'))
    mpl.rcParams['legend.fontsize'] = 10
    fig = plt.figure()
    ax = fig.gca(projection='3d')
    pts = t.points()
    ax.autoscale(enable=False)
    ax.plot(pts[:, 0], pts[:, 1], pts[:, 2], zdir='y', label='trajectory')
    ax.legend()
    print ax.get_w_lims()
    plt.show()
