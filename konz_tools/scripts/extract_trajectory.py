#!/usr/bin/env python

import argparse

PACKAGE = 'konz_tools'

import roslib
roslib.load_manifest(PACKAGE)

import rosbag
import geometry_msgs.msg
import tf
import rospy

from bag import Bag
from data_files import TrajectoryFile

if __name__ == '__main__':
    parser = argparse.ArgumentParser(description='''
    This script reads a bag file containing /tf topic and extracts a trajectory
    of a given frame in relation to another given frame, optionally applying
    additional transformation.

    Optional arguments allow to provide a name for the output file or a suffix
    to be used to construct the output file name automatically.
    ''', formatter_class=argparse.RawTextHelpFormatter)
    parser.add_argument('-t', '--target', required=True,
                        help='target frame id')
    parser.add_argument('-w', '--world', required=True,
                        help='world frame id')
    parser.add_argument('-s', '--suffix', default='trajectory',
                        help='trajectory filename suffix')
    parser.add_argument('-n', '--normalize', action='store_true',
                        help='transform the extracted trajectory so it starts '
                        'at the origin')
    parser.add_argument('--transform', nargs=7, type=float,
                        help='additional transform')
    parser.add_argument('bag', help='input bag file')
    parser.add_argument('output', nargs='?', help='output trajectory file')
    args = parser.parse_args()

    try:
        bag = Bag(args.bag)
    except Exception as e:
        exit("Error: %s" % e)

    if not args.output:
        args.output = bag.stem + args.suffix + ".txt"

    print "Processing bag file:"
    print "  in:", args.bag
    print "  out:", args.output
    print "  target frame id:", args.target
    print "  world frame id:", args.world
    print "  additional transform:", args.transform
    print "  normalize:", "yes" if args.normalize else "no"

    outfile = TrajectoryFile(args.output, 'w')
    outfile.write_header("trajectory of %s frame with respect to %s frame" %
                         (args.target, args.world), args.bag)

    # prepare an additional transformation
    temp_frame_id = '/TEMPORARY_TRANSFORM'
    m = geometry_msgs.msg.TransformStamped()
    m.header.frame_id = args.target
    m.child_frame_id = temp_frame_id
    if args.transform:
        m.transform.translation.x = args.transform[0]
        m.transform.translation.y = args.transform[1]
        m.transform.translation.z = args.transform[2]
        m.transform.rotation.x = args.transform[3]
        m.transform.rotation.y = args.transform[4]
        m.transform.rotation.z = args.transform[5]
        m.transform.rotation.w = args.transform[6]
    else:
        m.transform.rotation.w = 1.0
    args.target = temp_frame_id

    # prepare a transformation to change the origin
    new_origin_frame_id = '/NEW_ORIGIN'
    n = geometry_msgs.msg.TransformStamped()
    n.header.frame_id = args.world
    n.child_frame_id = new_origin_frame_id
    n.transform.rotation.w = 1.0

    inbag = rosbag.Bag(args.bag, 'r')

    time_start = None
    t = tf.Transformer(False, rospy.Duration(1.0))
    for topic, msg, time in inbag.read_messages(topics='/tf'):
        if time_start is None:
            time_start = time
        print "t = %.5f\r" % (time - time_start).to_sec(),
        has_world = False
        m.header.stamp = time
        t.setTransform(m)
        n.header.stamp = time
        t.setTransform(n)
        for transform in msg.transforms:
            t.setTransform(transform)
            if transform.header.frame_id == args.world:
                has_world = True
        if has_world and t.canTransform(new_origin_frame_id, args.target,
                                        rospy.Time()):
        #if t.canTransform(new_origin_frame_id, args.target, rospy.Time()):
            p, q = t.lookupTransform(new_origin_frame_id, args.target,
                                     rospy.Time())
            if args.normalize:
                n.transform.translation.x = p[0]
                n.transform.translation.y = p[1]
                n.transform.translation.z = p[2]
                n.transform.rotation.x = q[0]
                n.transform.rotation.y = q[1]
                n.transform.rotation.z = q[2]
                n.transform.rotation.w = q[3]
                args.normalize = False
                continue
            q = (q[3], q[0], q[1], q[2])
            outfile.write_line(time.to_sec(), (p, q))
    outfile.close()
