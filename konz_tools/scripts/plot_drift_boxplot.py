#!/usr/bin/env python

import argparse
import numpy as np
import matplotlib.pyplot as plt
from matplotlib import rc
rc('text', usetex=True)
rc('font', family='serif', size=8)

PACKAGE = 'konz_tools'

import roslib
roslib.load_manifest(PACKAGE)


class DriftBoxplot:

    def __init__(self, data1, data2, labels=None):
        if not len(data1) == len(data2):
            raise Exception('Data length mismatch.')
        self.labels = labels or ['method%d' % d for d in range(len(data1))]
        self.fig, self.ax = plt.subplots(1, 2, sharex=False, sharey=True)
        for a, d in zip(self.ax, [data1, data2]):
            a.boxplot(d, vert=0, widths=0.65)
            a.yaxis.set_ticklabels(self.labels)
            a.grid(True)
            a.set_xscale('log')
        self.ax[0].yaxis.tick_right()
        self.ax[0].set_title('Translational drift (m/s)')
        self.ax[1].set_title('Rotational drift (deg/s)')
        self.fig.tight_layout()

    def show(self):
        self.fig.show()

    def save_pdf(self, name, width=4.8, height=None, bg='white'):
        if width:
            self.fig.set_figwidth(width)
        if height:
            self.fig.set_figheight(height)
        self.fig.tight_layout()
        self.fig.savefig(name + '.pdf', dpi=300, format='pdf', facecolor=bg)


if __name__ == '__main__':
    parser = argparse.ArgumentParser(description='''
    Plot translational and rotational drift as a twinned boxplot.
    ''')
    parser.add_argument('drift', nargs='+',
                        help='drift file(s)')
    args = parser.parse_args()

    trans = list()
    rot = list()
    for d in args.drift:
        data = np.loadtxt(d)
        trans.append(data[:, 1])
        rot.append(data[:, 2])

    plot = DriftBoxplot(trans, rot)
    plt.show()
