#!/usr/bin/env python

import os
import sys
import argparse
import numpy as np

PACKAGE = 'konz_tools'

import roslib
roslib.load_manifest(PACKAGE)

from bag import Bag
from filesystem import Folder
import trajectory

from progress_bar import ProgressBar
from evaluation_tasks import OdometryEvaluationTask
from data_files import ErrorsFile, get_line_count


if __name__ == '__main__':
    parser = argparse.ArgumentParser(description='''
    This script will run the selected odometry evaluation program on the given
    bag file. It supplies all the necessary options automatically.

    Output goes to '$KONZ_EXPERIMENTS/odom/{method}/planar' folder.
    ''', formatter_class=argparse.RawTextHelpFormatter)
    parser.add_argument('method', help='odometry method')
    parser.add_argument('bag', help='relative path to the bag file')
    parser.add_argument('--config', help='config file (relative to the config/'
                        ' folder of the respective odometry package)')
    args = parser.parse_args()

    try:
        task = OdometryEvaluationTask(args.method)
        bag = Bag(args.bag)
    except Exception as e:
        exit("Error: %s" % e)

    if not bag.dataset.name == "brsu":
        exit("Planar odometry only supported with BRSU dataset.")

    folder = Folder(os.path.join(os.environ['KONZ_EXPERIMENTS'], 'odom',
                                 args.method, 'planar'))
    output = folder('%s.trajectory' % bag.stem)

    task.set_output(output)
    task.set_bag(bag)
    if args.config:
        task.set_config(args.config, relative=True)
    task.set_planar(True)
    task.set_camera_orientation(bag.dataset.pose['rotation'])

    print task
    print '\nLaunching...\n'
    task.run()

    print '\nDone. Computing errors...\n'
    groundtruth = bag.load_groundtruth_trajectory()
    estimated = trajectory.load(open(output, 'r'))
    from metrics import drift_per_second
    err = drift_per_second(estimated, groundtruth)
    np.savetxt(folder('%s.dps' % bag.stem), err)

    def print_stats(title, data, unit='', out=sys.__stdout__):
        print >> out, title
        print >> out, "# RMSE    %f %s" % (np.sqrt(np.dot(data, data) / len(data)), unit)
        print >> out, "# Mean    %f %s" % (np.mean(data), unit)
        print >> out, "# Median  %f %s" % (np.median(data), unit)
        print >> out, "# Std     %f %s" % (np.std(data), unit)
        print >> out, "# Min     %f %s" % (np.min(data), unit)
        print >> out, "# Max     %f %s" % (np.max(data), unit)
        print >> out, "# Poses   %i" % len(data)

    print_stats('Drift per second', err[:, 1], unit='m')
    print_stats('Drift per second', err[:, 2], unit='rad')
    #err = drift_per_frame(estimated, groundtruth)
    #np.savetxt(folder('%s.dpf' % bag.stem), err)
    #print_stats('Drift per frame', err[:, 1], unit='m')

    print '\nComputing drift per frame...\n'
    errors = ErrorsFile(folder('%s.dpf' % bag.stem), 'w')
    errors.write_header(args.method, bag.basename)
    bar = ProgressBar(get_line_count(output) - 3, 30)
    for ts1, ts2 in zip(estimated.timestamps[:-1], estimated.timestamps[1:]):
        bar.update()
        try:
            et = estimated.between(ts2, ts1)
            gt = groundtruth.between(ts2, ts1)
        except:
            continue  # extrapolation error in groundtruth trajectory
        errors.write_line(gt, et, 0, False)
    bar.done()
    errors.close()
