#!/usr/bin/env python

PACKAGE = 'konz_tools'

import roslib
roslib.load_manifest(PACKAGE)

from bag import Bag
from evaluate_transform_computer import evaluate_transform_computer

SEQUENCES = ['fr1/rgbd_dataset_freiburg1_xyz.bag',
             'fr1/rgbd_dataset_freiburg1_rpy.bag',
             'fr1/rgbd_dataset_freiburg1_room.bag',
             'fr1/rgbd_dataset_freiburg1_floor.bag',
             'fr1/rgbd_dataset_freiburg1_desk.bag',
             'fr1/rgbd_dataset_freiburg1_desk2.bag',
             'fr1/rgbd_dataset_freiburg1_360.bag',
             'fr2/rgbd_dataset_freiburg2_xyz.bag',
             'fr2/rgbd_dataset_freiburg2_rpy.bag',
             'fr2/rgbd_dataset_freiburg2_desk.bag',
             'fr2/rgbd_dataset_freiburg2_360_hemisphere.bag',
             'fr2/rgbd_dataset_freiburg2_360_kidnap.bag',
             'fr2/rgbd_dataset_freiburg2_large_no_loop.bag',
             'fr2/rgbd_dataset_freiburg2_large_with_loop.bag',
             'fr3/rgbd_dataset_freiburg3_long_office_household.bag']

if __name__ == '__main__':
    for s in SEQUENCES:
        bag = Bag('konz_datasets/data/' + s)
        for method in ['fovis', 'dvo', 'pm']:
            evaluate_transform_computer(method, bag)
