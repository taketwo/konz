#!/usr/bin/env python

import argparse
import subprocess

PACKAGE = 'konz_tools'

import roslib
roslib.load_manifest(PACKAGE)

from bag import Bag


def decompose_pointcloud_messages(bag, cloud_topic):
    import os.path
    import rosbag
    cmd = 'rosrun %s %s %s %s %s %s %s' % (PACKAGE, 'pointcloud_decomposer',
                                           bag.abspath, cloud_topic,
                                           bag.dataset.topics[0],
                                           bag.dataset.topics[1],
                                           bag.dataset.topics[2])
    print 'Decomposing pointclouds and writing to a temporary bag file...'
    subprocess.call(cmd.split())
    old = bag.abspath + '.old'
    out = bag.abspath + '.out'
    os.rename(bag.abspath, old)
    print 'Merging into original bag file...'
    with rosbag.Bag(bag.abspath, 'w') as outbag:
        with rosbag.Bag(old, 'r') as inbag:
            for t, msg, ts in inbag.read_messages():
                if t != cloud_topic:
                    outbag.write(t, msg, ts)
        with rosbag.Bag(out, 'r') as inbag:
            for t, msg, ts in inbag.read_messages():
                outbag.write(t, msg, ts)
    os.remove(old)
    os.remove(out)

if __name__ == '__main__':
    parser = argparse.ArgumentParser(description='''
    This script will decompose all the messages in the given point cloud topic
    into color, depth, and camera info messages and write them back to the bag
    file. The original point cloud messages will be discarded, and all other
    messages will be kept.
    ''', formatter_class=argparse.RawDescriptionHelpFormatter)
    parser.add_argument('bag', help='relative path to the bag file')
    parser.add_argument('topic', help='point cloud topic')
    args = parser.parse_args()

    try:
        bag = Bag(args.bag)
    except Exception as e:
        exit("Error: %s" % e)

    decompose_pointcloud_messages(bag, args.topic)
