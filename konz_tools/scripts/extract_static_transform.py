#!/usr/bin/env python

import argparse

PACKAGE = 'konz_tools'

import roslib
roslib.load_manifest(PACKAGE)

import rosbag
import tf
import rospy

from bag import Bag
from transform import Transform

if __name__ == '__main__':
    parser = argparse.ArgumentParser(description='''
    This script reads a bag file containing /tf topic and extracts a transform
    between two given frames under the assumption that it is static.
    ''', formatter_class=argparse.RawTextHelpFormatter)
    parser.add_argument('-t', '--target', required=True,
                        help='target frame id')
    parser.add_argument('-w', '--world', required=True,
                        help='world frame id')
    parser.add_argument('bag', help='input bag file')
    args = parser.parse_args()

    try:
        bag = Bag(args.bag)
    except Exception as e:
        exit("Error: %s" % e)

    print 'Looking up the transform...'
    T = None
    inbag = rosbag.Bag(args.bag, 'r')
    t = tf.Transformer(False, rospy.Duration(1.0))
    for topic, msg, time in inbag.read_messages(topics='/tf'):
        for transform in msg.transforms:
            t.setTransform(transform)
        if t.canTransform(args.world, args.target, rospy.Time()):
            p, q = t.lookupTransform(args.world, args.target, rospy.Time())
            q = (q[3], q[0], q[1], q[2])
            T = Transform.from_translation_and_rotation(p, q)
            break
    if T:
        print 'Found transform:'
        print 'Translation', T.translation
        print 'Rotation RPY', T.euler
        print 'Rotation Quaternion', T.quaternion
    else:
        print 'Unable to find the transform between two given frames.'
