#!/usr/bin/env python

import subprocess

PACKAGE = 'konz_tools'

import roslib
roslib.load_manifest(PACKAGE)

from dataset import Dataset

if __name__ == '__main__':
    cmd = ('rosrun konz_tools evaluate_transform_computer.py '
           '--config 160x120.yaml %s %s')
    ds = Dataset('ethz')
    for bag in ds.get_bags():
        for method in ['fovis', 'dvo', 'pm']:
            print 'Evaluating %s with %s' % (method, bag.basename)
            subprocess.call((cmd % (method, bag.abspath)).split())
