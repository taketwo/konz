#!/usr/bin/env python

import argparse

PACKAGE = 'konz_tools'

import roslib
roslib.load_manifest(PACKAGE)

from bag import Bag
from progress_bar import ProgressBar


def replace_camera_info_messages(bag, D=None, K=None, P=None):
    import os.path
    import rosbag
    D = D or [0.0, 0.0, 0.0, 0.0, 0.0]
    K = K or [525.0, 0.0, 319.5, 0.0, 525.0, 239.5, 0.0, 0.0, 1.0]
    P = P or [K[0], 0.0, K[2], 0.0, 0.0, K[4], K[5], 0.0, 0.0, 0.0, 1.0, 0.0]
    topic = bag.dataset.topics[-1]
    msg_count = bag.count_messages_in_topic(topic)
    old = bag.abspath + '.old'
    os.rename(bag.abspath, old)
    print 'Replacing camera info messages...'
    bar = ProgressBar(msg_count, 30)
    with rosbag.Bag(bag.abspath, 'w') as outbag:
        with rosbag.Bag(old, 'r') as inbag:
            for t, msg, ts in inbag.read_messages():
                if t == topic:
                    bar.update()
                    msg.D = D
                    msg.K = K
                    msg.P = P
                    outbag.write(t, msg, ts)
                else:
                    outbag.write(t, msg, ts)
    bar.done()
    os.remove(old)

if __name__ == '__main__':
    parser = argparse.ArgumentParser(description='''
    This script will replace the calibration parameters in all the camera info
    messages in the given bag file. The new calibration parameters are read
    from the dataset description.

    Note: the output bag will be non-compressed.
    ''', formatter_class=argparse.RawDescriptionHelpFormatter)
    parser.add_argument('bag', help='relative path to the bag file')
    args = parser.parse_args()

    try:
        bag = Bag(args.bag)
    except Exception as e:
        exit("Error: %s" % e)

    replace_camera_info_messages(bag, **bag.dataset.calibration)
