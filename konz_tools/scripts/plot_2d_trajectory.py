#!/usr/bin/env python

import argparse
import matplotlib.pyplot as plt
from matplotlib import rc
rc('text', usetex=True)
rc('font', family='serif', size=8)

PACKAGE = 'konz_tools'

import roslib
roslib.load_manifest(PACKAGE)

import trajectory


class TrajectoryPlot:

    STYLES = ['-g', '-r', '-b', '-k']
    LINEWIDTH = 1

    def __init__(self, title=None):
        self.fig = plt.figure()
        self.ax = plt.axes()
        if title is not None:
            self.ax.set_title(title)
        self.xlim = None
        self.ylim = None
        self.padding = 0.2
        self.make_grid(1.0, 0.5)
        self.trajectories = list()

    def show(self):
        self.fig.show()

    def add_trajectory(self, trajectory, style=None, label=None):
        if style is None:
            style = (self.STYLES[len(self.trajectories)], self.LINEWIDTH)
        if label is None:
            label = 'trajectory %d' % (len(self.trajectories) + 1)
        self.trajectories.append(trajectory)
        points = trajectory.points()
        if isinstance(style, str):
            self.ax.plot(points[:, 0], points[:, 1], style, label=label,
                         linewidth=self.LINEWIDTH)
        else:
            self.ax.plot(points[:, 0], points[:, 1], style[0], label=label,
                         linewidth=style[1])
        tmax = points.max(axis=0)
        tmin = points.min(axis=0)
        self.update_limits((tmin[0], tmax[0]), (tmin[1], tmax[1]))

    def update_limits(self, xlim, ylim):
        xlim = (xlim[0] - self.padding, xlim[1] + self.padding)
        ylim = (ylim[0] - self.padding, ylim[1] + self.padding)
        self.xlim = self.xlim or xlim
        self.xlim = (min(self.xlim[0], xlim[0]), max(self.xlim[1], xlim[1]))
        self.ylim = self.ylim or ylim
        self.ylim = (min(self.ylim[0], ylim[0]), max(self.ylim[1], ylim[1]))
        self.ax.set_xlim(*self.xlim)
        self.ax.set_ylim(*self.ylim)

    def save_pdf(self, name, width=4.8, height=None, bg='white'):
        xwidth = self.xlim[1] - self.xlim[0]
        ywidth = self.ylim[1] - self.ylim[0]
        ratio = ywidth / xwidth
        height = height or width * ratio
        self.fig.set_figwidth(width)
        self.fig.set_figheight(height)
        self.fig.savefig(name + '.pdf', dpi=300, format='pdf', facecolor=bg)

    def make_grid(self, major=1.0, minor=0.1):
        self.ax.xaxis.set_major_locator(plt.MultipleLocator(major))
        self.ax.xaxis.set_minor_locator(plt.MultipleLocator(minor))
        self.ax.yaxis.set_major_locator(plt.MultipleLocator(major))
        self.ax.yaxis.set_minor_locator(plt.MultipleLocator(minor))
        self.ax.grid(which='major', axis='x',
                     linewidth=0.5, linestyle='-', color='0.75')
        self.ax.grid(which='minor', axis='x',
                     linewidth=0.1, linestyle='-', color='0.75')
        self.ax.grid(which='major', axis='y',
                     linewidth=0.5, linestyle='-', color='0.75')
        self.ax.grid(which='minor', axis='y',
                     linewidth=0.1, linestyle='-', color='0.75')
        self.ax.set_autoscale_on(False)
        self.ax.set_axisbelow(True)

if __name__ == '__main__':
    parser = argparse.ArgumentParser(description='''
    Plot 2d trajectory.
    ''')
    parser.add_argument('trajectory', nargs='+',
                        help='trajectory file(s)')
    args = parser.parse_args()

    plot = TrajectoryPlot()
    for t in args.trajectory:
        plot.add_trajectory(trajectory.load(open(t, 'r')))

    plt.show()
