#!/usr/bin/env python

import os
import argparse

PACKAGE = 'konz_tools'

import roslib
roslib.load_manifest(PACKAGE)

from dataset import Dataset
import a

if __name__ == '__main__':
    parser = argparse.ArgumentParser(description='''
    This script produces a boxplot for the files 'xxx.errors' in the
    current directory, where 'xxx' is a tracker name.
    ''')
    parser.add_argument('--folder', help='base folder with odometry results')
    args = parser.parse_args()

    if not args.folder:
        args.folder = os.path.join(os.environ['KONZ_EXPERIMENTS'], 'odom')

    D = a.OdomData.load_dataset(args.folder)

    for d in ['fr1', 'fr2', 'fr3']:
        print 'Processing dataset %s...' % d
        for b in Dataset(d).get_bag_names():
            print 'Plotting results for bag %s...' % b
            a.plot_box_plots(D, b, save=True)
