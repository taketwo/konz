#!/usr/bin/env python

import argparse

PACKAGE = 'konz_tools'

import roslib
roslib.load_manifest(PACKAGE)

import rosbag

from bag import Bag
from progress_bar import ProgressBar
import trajectory
from data_files import IntervalsFile


def create_intervals_file(bag, distance, angle):
    print 'Will create intervals file for bag %s.' % bag.stem
    print 'Loading ground truth trajectory...'
    traj = trajectory.load(open(bag.groundtruth, 'r'))

    print 'Loading data timestamps from the bag file...'
    data_timestamps = list()
    bar = ProgressBar(bag.count_messages_in_topic(bag.dataset.topics[0]), 30)
    with rosbag.Bag(bag.abspath) as b:
        for topic, msg, ts in b.read_messages(topics=[bag.dataset.topics[0]]):
            bar.update()
            data_timestamps.append(ts.to_sec())
    bar.done()

    print 'Computing interpolated ground truth poses...'
    poses = list()
    timestamps = list()
    bar = ProgressBar(len(data_timestamps), 30)
    for i in xrange(len(data_timestamps)):
        bar.update(i)
        try:
            poses.append(traj.at(data_timestamps[i]))
            timestamps.append(data_timestamps[i])
        except:
            pass  # interpolation or extrapolation exception, skip timestamp
    bar.done()

    print 'Computing intervals...'
    bar = ProgressBar(len(timestamps), 30)
    with IntervalsFile(bag.intervals, 'w') as intervals:
        intervals.write_header(distance, angle, bag.basename)
        for i in xrange(len(timestamps)):
            bar.update(i)
            j = i + 1
            while j < len(timestamps):
                tf = poses[i].transform_to(poses[j])
                if tf.distance() > distance or tf.angle() > angle:
                    break
                j += 1
            if not i == j - 1:
                intervals.write_line(timestamps[i], timestamps[j - 1])
    bar.done()


if __name__ == '__main__':
    parser = argparse.ArgumentParser(description='''
    This script will create intervals file for the given bag. The interval
    length is determined by the maximum distance and angle between poses in it.
    The intervals file is output to the 'intervals/' folder of the dataset.
    ''', formatter_class=argparse.RawDescriptionHelpFormatter)
    parser.add_argument('--distance', type=float, default=0.3,
                        help='max distance')
    parser.add_argument('--angle', type=float, default=0.15,
                        help='max angle')
    parser.add_argument('bag', help='relative path to the bag file')
    args = parser.parse_args()

    try:
        bag = Bag(args.bag)
    except Exception as e:
        exit("Error: %s" % e)

    create_intervals_file(bag, args.distance, args.angle)
