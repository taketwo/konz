#!/usr/bin/env python

import os
import argparse
import numpy as np

PACKAGE = 'konz_tools'

import roslib
roslib.load_manifest(PACKAGE)

from bag import Bag
from filesystem import Folder
import trajectory

from progress_bar import ProgressBar
from evaluation_tasks import OdometryEvaluationTask
from data_files import ErrorsFile, get_line_count
from metrics import drift_per_second, print_stats


def evaluate_odometry(method, bag, config='', planar=False):
    if planar and not bag.dataset.name == 'brsu':
        raise Exception('Planar odometry only supported with BRSU dataset.')

    task = OdometryEvaluationTask(method)
    folder = Folder(os.path.join(os.environ['KONZ_EXPERIMENTS'], 'odom',
                                 method, 'planar' if planar else '', config))
    output = folder('%s.trajectory' % bag.stem)
    task.set_output(output)
    task.set_bag(bag)
    if config:
        task.set_config(config, relative=True)
    if planar:
        task.set_planar(True)
        task.set_camera_orientation(bag.dataset.pose['rotation'])

    print task
    print '\nLaunching...\n'
    task.run()

    print '\nDone. Computing errors...\n'
    groundtruth = bag.load_groundtruth_trajectory()
    estimated = trajectory.load(open(output, 'r'))
    dps = open(folder('%s.dps' % bag.stem), 'w')
    err = drift_per_second(estimated, groundtruth)

    print_stats('# Trans. drift per second', err[:, 1], unit='m', out=dps)
    print_stats('# Trans. drift per second', err[:, 1], unit='m')
    print_stats('# Rot. drift per second', err[:, 2], unit='rad', out=dps)
    print_stats('# Rot. drift per second', err[:, 2], unit='rad')
    np.savetxt(dps, err)

    print '\nComputing drift per frame...\n'
    errors = ErrorsFile(folder('%s.dpf' % bag.stem), 'w')
    errors.write_header(method, bag.basename)
    bar = ProgressBar(get_line_count(output) - 3, 30)
    for ts1, ts2 in zip(estimated.timestamps[:-1], estimated.timestamps[1:]):
        bar.update()
        try:
            et = estimated.between(ts2, ts1)
            gt = groundtruth.between(ts2, ts1)
        except:
            continue  # extrapolation error in groundtruth trajectory
        errors.write_line(gt, et, 0, False)
    bar.done()
    errors.close()
    pass

if __name__ == '__main__':
    parser = argparse.ArgumentParser(description='''
    This script will run the selected odometry evaluation program on the given
    bag file. It supplies all the necessary options automatically.

    Output goes to '$KONZ_EXPERIMENTS/odom/{method}/' folder.
    ''', formatter_class=argparse.RawTextHelpFormatter)
    parser.add_argument('method', help='odometry method')
    parser.add_argument('bag', help='relative path to the bag file')
    parser.add_argument('--config', default='',
                        help='config file (relative to the config/ folder of '
                        'the respective odometry package)')
    args = parser.parse_args()

    try:
        bag = Bag(args.bag)
    except Exception as e:
        exit("Error: %s" % e)

    evaluate_odometry(args.method, bag, args.config)
