#!/usr/bin/env python

import os
import argparse

PACKAGE = 'konz_tools'

import roslib
roslib.load_manifest(PACKAGE)

from bag import Bag
from filesystem import Folder
from progress_bar import ProgressBar

from evaluation_tasks import TransformComputerEvaluationTask
from data_files import TransformsFile, ErrorsFile, get_line_count


def evaluate_transform_computer(method, bag, config=None, limit=None):
    task = TransformComputerEvaluationTask(method)
    folder = Folder(os.environ['KONZ_EXPERIMENTS'] + '/tc/' + method)
    transforms = folder('%s.transforms' % bag.stem)
    task.set_intervals(bag.intervals)
    task.set_output(transforms)
    task.set_bag(bag)
    if config is not None:
        task.set_config(config, relative=True)
    if limit is not None:
        task.set_limit(limit)

    print task
    print '\nLaunching...\n'
    task.run()

    print '\nDone. Computing errors...\n'
    groundtruth = bag.load_groundtruth_trajectory()
    errors = ErrorsFile(folder('%s.errors' % bag.stem), 'w')
    errors.write_header(method, bag.basename)
    bar = ProgressBar(get_line_count(transforms) - 3, 30)
    with open(transforms, 'r') as transforms:
        for line in transforms:
            bar.update()
            try:
                ts1, ts2, et, rt, fail = TransformsFile.parse_line(line)
                gt = groundtruth.between(ts1, ts2)
            except:
                continue
            errors.write_line(gt, et, rt, fail)
    bar.done()
    errors.close()


if __name__ == '__main__':
    parser = argparse.ArgumentParser(description='''
    This script will run the selected transform computation evaluation program
    on the given bag file. It supplies all the necessary options automatically.
    After that the error in each transform is computed and stored.

    Output goes to '$KONZ_EXPERIMENTS/tc/{method}/{bag}.errors' file.
    ''', formatter_class=argparse.RawTextHelpFormatter)
    parser.add_argument('method', help='transform computation method')
    parser.add_argument('bag', help='relative path to the bag file')
    parser.add_argument('--config', help='config file (relative to the config/'
                        ' folder of the respective transform computation'
                        ' package)')
    parser.add_argument('--limit', help='limit the number of intervals')
    args = parser.parse_args()

    try:
        bag = Bag(args.bag)
    except Exception as e:
        exit("Error: %s" % e)

    evaluate_transform_computer(args.method, bag, args.config, args.limit)
