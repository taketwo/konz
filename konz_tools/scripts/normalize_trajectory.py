#!/usr/bin/env python

import os
import argparse

PACKAGE = 'konz_tools'

import roslib
roslib.load_manifest(PACKAGE)

import trajectory

if __name__ == '__main__':
    parser = argparse.ArgumentParser(description='''
    This script reads the trajectory from the input file, normalizes it (i.e.
    applies a constant transform to each if its poses so that the very first
    pose coincides with the origin), and stores to the output file.
    ''', formatter_class=argparse.RawTextHelpFormatter)
    parser.add_argument('infile', help='input trajectory file')
    parser.add_argument('outfile', nargs='?', help='output trajectory file')
    args = parser.parse_args()

    if not args.outfile:
        args.outfile = os.path.splitext(args.infile)[0] + "-normalized.txt"

    traj = trajectory.load(open(args.infile, 'r'))
    traj.transform(traj.at(traj.timestamps[0]).inverse())
    trajectory.save(open(args.outfile, 'w'), traj)
