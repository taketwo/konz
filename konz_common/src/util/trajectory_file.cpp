#include <boost/filesystem.hpp>
#include <boost/format.hpp>

#include "konz_common/util/trajectory_file.h"

namespace konz
{

namespace common
{

namespace util
{

TrajectoryFile::TrajectoryFile(const std::string& filename)
: file_stream_(filename)
{
}

TrajectoryFile::~TrajectoryFile()
{
  file_stream_.close();
}

void TrajectoryFile::writeHeader(const std::string& title, const std::string& bag_filename)
{
  boost::filesystem::path p(bag_filename);
  file_stream_ << "# " << title << "\n";
  file_stream_ << "# file: '" << p.filename().string() << "'\n";
  file_stream_ << "# timestamp tx ty tz qx qy qz qw failed\n";
}

void TrajectoryFile::writeLine(double timestamp, const Transform& transform, bool failed)
{
  const Quaternion q(transform.rotation());
  const Vector3 t(transform.translation());
  file_stream_ << boost::format("%.4f %.4f %.4f %.4f %.4f %.4f %.4f %.4f%s\n") % timestamp                     \
                                                                               % t[0] % t[1] % t[2]            \
                                                                               % q.x() % q.y() % q.z() % q.w() \
                                                                               % (failed ? " #" : "");
}

}

}

}

