#include <boost/filesystem.hpp>
#include <boost/format.hpp>

#include "konz_common/util/transforms_file.h"

namespace konz
{

namespace common
{

namespace util
{

TransformsFile::TransformsFile(const std::string& filename)
: file_stream_(filename)
{
}

TransformsFile::~TransformsFile()
{
  file_stream_.close();
}

void TransformsFile::writeHeader(const std::string& title, const std::string& bag_filename)
{
  boost::filesystem::path p(bag_filename);
  file_stream_ << "# " << title << "\n";
  file_stream_ << "# file: '" << p.filename().string() << "'\n";
  file_stream_ << "# timestamp1 timestamp2 tx ty tz qx qy qz qw succeeded runtime\n";
}

void TransformsFile::writeLine(double timestamp1, double timestamp2, const Transform& transform, double runtime, bool succeeded)
{
  const Quaternion q(transform.rotation());
  const Vector3 t(transform.translation());
  file_stream_ << boost::format("%.4f %.4f %.4f %.4f %.4f %.4f %.4f %.4f %.4f %.4f%s\n") % timestamp1 % timestamp2       \
                                                                                         % t[0] % t[1] % t[2]            \
                                                                                         % q.x() % q.y() % q.z() % q.w() \
                                                                                         % runtime                       \
                                                                                         % (succeeded ? " #" : "");
}

}

}

}

