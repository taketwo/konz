#include <iostream>
#include <fstream>
#include <sstream>

#include "konz_common/util/interval.h"

namespace konz
{

namespace common
{

namespace util
{

Intervals readIntervalsFile(const std::string& filename)
{
  std::vector<Interval> intervals;
  std::ifstream stream(filename);
  std::string line;
  double s, e;
  if (stream)
    while (getline(stream,line))
    {
      if (!line.size() || line[0] == '#')
        continue;
      std::stringstream(line) >> s >> e;
      intervals.push_back(s < e ? Interval{s, e} : Interval{e, s});
    }
  stream.close();
  return intervals;
}

}

}

}

