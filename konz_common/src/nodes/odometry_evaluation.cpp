#include <cstdio>

#include <boost/program_options.hpp>

#include "konz_common/util/timer.h"
#include "konz_common/util/trajectory_file.h"
#include "konz_common/util/console_frame_indicator.h"
#include <konz_common/transform_filters/planar_transform_filter.h>

#include "konz_common/nodes/odometry_evaluation.h"

namespace konz
{

namespace common
{

namespace nodes
{

bool OdometryEvaluation::init(int argc, char* argv[])
{
  using namespace std;
  namespace po = boost::program_options;

  // Step 1: parse command-line arguments
  po::options_description opt("Allowed options");
  opt.add_options()
      ("config,c", po::value<string>(&args_.config)->required(),
       "odometry configuration file")
      ("output,o", po::value<string>(&args_.output)->required(),
       "output estimated trajectory file")
      ("limit,l", po::value<int>(&args_.limit)->default_value(-1),
       "limit evaluation by a number of first frames (-1 means no limit)")
      ("planar,p", po::value<bool>(&args_.planar)->default_value(false)->zero_tokens(),
       "enforce planar motion constraints")
      ("camera-orientation,q", po::value<string>(&args_.camera_orientation)->default_value("1 0 0 0"),
       "orientation of the camera w.r.t. robot's frame as a quaternian (\"w x y z\")")
      ("bag,b", po::value<string>(&args_.bag)->required(),
       "bag file")
      ("topics,t", po::value<vector<string>>(&args_.topics)->required()->multitoken(),
       "color, depth, and camera info topic names")
      ("help", "produce help message");
  po::positional_options_description pos;
  pos.add("topics", 3);
  po::variables_map vm;
  po::store(po::command_line_parser(argc, argv).options(opt).positional(pos).run(), vm);
  if (vm.count("help"))
  {
    std::cout << opt;
    return false;
  }
  else
  {
    po::notify(vm);
    if (args_.topics.size() != 3)
      throw po::error("three topic names required");
    if (sscanf(args_.camera_orientation.c_str(), "%f %f %f %f", &args_.qw, &args_.qx, &args_.qy, &args_.qz) != 4)
      throw po::error("invalid camera orientation option");
  }

  // Step 2: open files
  bag_.open(args_.bag.c_str());

  // Step 3: initialize members
  emitter_ = emitters::BagFrameEmitter::UPtr(new emitters::BagFrameEmitter(bag_, args_.topics.at(0), args_.topics.at(1), args_.topics.at(2)));
  initOdometry();

  if (!odometry_)
    throw std::runtime_error("odometry left uninitialized");
  if (args_.planar)
  {
    Quaternion q(args_.qw, args_.qx, args_.qy, args_.qz);
    odometry_->setTransformFilter(std::make_shared<transform_filters::PlanarTransformFilter>(q));
  }

  return true;
}

void OdometryEvaluation::run()
{
  util::ConsoleFrameIndicator indicator;
  util::TrajectoryFile trajectory_file(args_.output);
  trajectory_file.writeHeader("odometry evaluation", args_.bag);

  Frame::Ptr frame = emitter_->next();

  int frame_count = 0;
  while (frame)
  {
    double runtime;

    {
      util::Timer t;
      odometry_->process(frame);
      runtime = t.elapsed();
    }

    Transform transform = odometry_->getPose();

    bool failed = odometry_->estimationFailedAtLastCall();
    bool keyframe = odometry_->keyframeCreatedAtLastCall();
    trajectory_file.writeLine(frame->getTimestamp(), transform, failed);
    indicator.frame(!failed, keyframe);
    frame = emitter_->next();

    if (args_.limit > 0 && ++frame_count >= args_.limit)
      break;
  }

  std::cout << std::endl;
  bag_.close();
}

}

}

}

