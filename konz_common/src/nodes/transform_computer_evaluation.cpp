#include <cstdio>

#include <boost/program_options.hpp>

#include "konz_common/util/interval.h"
#include "konz_common/util/timer.h"
#include "konz_common/util/transforms_file.h"

#include "konz_common/nodes/transform_computer_evaluation.h"

namespace konz
{

namespace common
{

namespace nodes
{

bool TransformComputerEvaluation::init(int argc, char* argv[])
{
  using namespace std;
  namespace po = boost::program_options;

  // Step 1: parse command-line arguments
  po::options_description opt("Allowed options");
  opt.add_options()
      ("config,c", po::value<string>(&args_.config)->required(),
       "transform computer configuration file")
      ("intervals,i", po::value<string>(&args_.intervals)->required(),
       "intervals file")
      ("output,o", po::value<string>(&args_.output)->required(),
       "output estimated transforms file")
      ("limit,l", po::value<int>(&args_.limit)->default_value(-1),
       "limit evaluation by a number of first intervals (-1 means no limit)")
      ("bag,b", po::value<string>(&args_.bag)->required(),
       "bag file")
      ("topics,t", po::value<vector<string>>(&args_.topics)->required()->multitoken(),
       "color, depth, and camera info topic names")
      ("help", "produce help message");
  po::positional_options_description pos;
  pos.add("topics", 3);
  po::variables_map vm;
  po::store(po::command_line_parser(argc, argv).options(opt).positional(pos).run(), vm);
  if (vm.count("help"))
  {
    std::cout << opt;
    return false;
  }
  else
  {
    po::notify(vm);
    if (args_.topics.size() != 3)
      throw po::error("three topic names required");
  }

  // Step 2: open files
  bag_.open(args_.bag.c_str());

  // Step 3: initialize members
  emitter_ = emitters::BagFrameEmitter::UPtr(new emitters::BagFrameEmitter(bag_, args_.topics.at(0), args_.topics.at(1), args_.topics.at(2)));
  initTransformComputer();

  if (!transform_computer_)
    throw std::runtime_error("transform computer left uninitialized");

  return true;
}

void TransformComputerEvaluation::run()
{
  util::Intervals intervals = util::readIntervalsFile(args_.intervals);
  util::ConsoleIntervalIndicator indicator(intervals.size());
  util::TransformsFile transforms_file(args_.output);
  transforms_file.writeHeader("transform computation evaluation", args_.bag);

  FrameQueue frames(*this);

  for (size_t i = 0; i < intervals.size(); i++)
  {
    indicator.interval(intervals[i]);

    Frame::Ptr first_frame = frames.pop(intervals[i]);

    if (!first_frame)
      continue;

    transform_computer_->setReferenceFrame(first_frame);
    Frame::Ptr frame = frames.next();

    while (frame)
    {
      Transform transform;
      double runtime;
      bool succeeded;

      {
        util::Timer t;
        succeeded = transform_computer_->compute(frame, transform);
        runtime = t.elapsed();
      }

      transforms_file.writeLine(first_frame->getTimestamp(), frame->getTimestamp(), transform, runtime, succeeded);
      indicator.transform(succeeded);
      frame = frames.next();
    }

    if (args_.limit > 0 && i + 1 >= static_cast<size_t>(args_.limit))
      break;
  }

  std::cout << std::endl;
  bag_.close();
}

TransformComputerEvaluation::FrameQueue::FrameQueue(TransformComputerEvaluation& parent)
: parent_(parent)
{
}

Frame::Ptr TransformComputerEvaluation::FrameQueue::pop(util::Interval interval)
{
  interval_ = interval;
  index_ = 0;

  // Remove all the frames in the beginning of the deque that are too old
  while (deque_.size() && deque_.front()->getTimestamp() < interval.start)
    deque_.pop_front();

  // If there remain frames in the deque
  if (deque_.size())
  {
    // And the first of them is in the requested interval
    if (interval.contains(deque_.front()->getTimestamp()))
    {
      // Pop it and return
      Frame::Ptr frame = deque_.front();
      deque_.pop_front();
      return frame;
    }
    else
    {
      return nullptr;
    }
  }

  // Here we know that the deque is empty, so milk the emitter
  Frame::Ptr frame = parent_.emitter_->next();

  // Keep on milking it as far as the frames are too old
  while (frame && frame->getTimestamp() < interval.start)
    frame = parent_.emitter_->next();

  // If we end up having a frame
  if (frame)
  {
    // And it is in the requested interval
    if (interval.contains(frame->getTimestamp()))
    {
      return frame;
    }
    else
    {
      // Otherwise it is from the future, so store it
      deque_.push_back(frame);
      return nullptr;
    }
  }

  // There are no frames at all (emitter is exhausted)
  return nullptr;
}

Frame::Ptr TransformComputerEvaluation::FrameQueue::next()
{
  while (index_ >= deque_.size())
  {
    Frame::Ptr frame = parent_.emitter_->next();
    if (frame)
      deque_.push_back(frame);
    else
      return nullptr;
  }

  if (interval_.contains(deque_[index_]->getTimestamp()))
    return deque_[index_++];
  else
    return nullptr;
}

}

}

}

