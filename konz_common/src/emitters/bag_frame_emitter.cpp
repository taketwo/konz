#include <ros/ros.h>

#include "konz_common/emitters/bag_frame_emitter.h"

namespace konz
{

namespace common
{

namespace emitters
{

BagFrameEmitter::BagFrameEmitter(const rosbag::Bag& bag, const std::string& color_t, const std::string& depth_t, const std::string& camera_info_t)
: bag_(bag)
, view_(bag_, rosbag::TopicQuery(std::vector<std::string>{color_t, depth_t}))
, view_iter_(view_.begin())
, color_topic_(color_t)
, depth_topic_(depth_t)
, synchronizer_(SyncPolicy(QUEUE_SIZE), color_subscriber_, depth_subscriber_)
{
  fetchCameraInfo(camera_info_t);
  synchronizer_.registerCallback(boost::bind(&BagFrameEmitter::synchronizerCallback, this, _1, _2));
  ros::Time::init();
}

void BagFrameEmitter::fetchCameraInfo(const std::string& info_t)
{
  rosbag::View info_view(bag_, rosbag::TopicQuery(info_t));
  if (info_view.size() == 0)
  {
    throw std::runtime_error("no camera info messages in bag file");
  }
  else
  {
    sensor_msgs::CameraInfoConstPtr ptr = (*info_view.begin()).instantiate<sensor_msgs::CameraInfo>();
    if (ptr == 0)
    {
      throw std::runtime_error("null camera info message after deserialization");
    }
    else
    {
      // we need a non-const copy so that it is possible to override camera calibration
      camera_info_msg_ = sensor_msgs::CameraInfoPtr(new sensor_msgs::CameraInfo(*ptr));
    }
  }
}

Frame::Ptr BagFrameEmitter::next()
{
  // until new frame is not generated and the end of bagfile is not reached
  while (!new_frame_ && view_iter_ != view_.end())
  {
    if (view_iter_->getTopic() == color_topic_)
    {
      sensor_msgs::ImageConstPtr color_img_ptr = (view_iter_++)->instantiate<sensor_msgs::Image>();
      if (color_img_ptr == 0)
        return nullptr;
      color_subscriber_.newMessage(color_img_ptr);
    }
    else if (view_iter_->getTopic() == depth_topic_)
    {
      sensor_msgs::ImageConstPtr depth_img_ptr = (view_iter_++)->instantiate<sensor_msgs::Image>();
      if (depth_img_ptr == 0)
        return nullptr;
      depth_subscriber_.newMessage(depth_img_ptr);
    }
  }

  // if there is a new frame, then return it
  if(new_frame_)
  {
    Frame::Ptr frame = new_frame_;
    new_frame_.reset();
    return frame;
  }
  else
  {
    return nullptr;
  }
}

void BagFrameEmitter::synchronizerCallback(const sensor_msgs::ImageConstPtr& color_img, const sensor_msgs::ImageConstPtr& depth_img)
{
  new_frame_ = std::make_shared<Frame>(color_img, depth_img, camera_info_msg_);
}

}

}

}

