#include <cstdio>
#include <string>
#include <iostream>
#include <fstream>

#include <gtest/gtest.h>

#include "konz_common/util/interval.h"

using namespace konz::common::util;

const char* INTERVALS = "test/intervals.txt";

class IntervalTest : public ::testing::Test
{

public:

  IntervalTest()
  {
    std::ofstream f(INTERVALS);
    f << "# header\n";
    f << "# another comment line\n";
    f << "3.0 4.0\n";
    f << "3.5 4.5\n";
    f << "# next interval has reversed bounds\n";
    f << "5.0 2.0";
    f.close();
  }

  ~IntervalTest()
  {
    std::remove(INTERVALS);
  }

};

TEST_F(IntervalTest, Contains)
{
  Interval i{10.0, 12.2};
  EXPECT_TRUE(i.contains(10.0));
  EXPECT_TRUE(i.contains(11.0));
  EXPECT_TRUE(i.contains(12.2));
  EXPECT_FALSE(i.contains(12.21));
  EXPECT_FALSE(i.contains(9.99));
  EXPECT_FALSE(i.contains(-10.0));
}

TEST_F(IntervalTest, ReadNonExistingIntervalsFile)
{
  Intervals is;
  ASSERT_NO_THROW(is = readIntervalsFile("test/random_file_name.txt"));
  ASSERT_EQ(0, is.size());
}

TEST_F(IntervalTest, ReadExistingIntervalsFile)
{
  Intervals is;
  ASSERT_NO_THROW(is = readIntervalsFile(INTERVALS));
  ASSERT_EQ(3, is.size());
  EXPECT_EQ(3.0, is[0].start);
  EXPECT_EQ(4.0, is[0].end);
  EXPECT_EQ(2.0, is[2].start);
  EXPECT_EQ(5.0, is[2].end);
}

int main(int argc, char **argv)
{
  testing::InitGoogleTest(&argc, argv);
  return RUN_ALL_TESTS();
}

