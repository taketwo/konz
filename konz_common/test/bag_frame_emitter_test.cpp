#include <cstdio>
#include <string>

#include <gtest/gtest.h>
#include <rosbag/bag.h>
#include <sensor_msgs/Image.h>
#include <sensor_msgs/CameraInfo.h>

#include "konz_common/emitters/bag_frame_emitter.h"

using namespace konz::common;
using namespace konz::common::emitters;

const std::string COLOR = "/camera/rgb/image_color";
const std::string DEPTH = "/camera/depth/image";
const std::string INFO = "/camera/rgb/camera_info";
const char* BAG = "test/tmp.bag";

class BagFrameEmitterTest : public ::testing::Test
{

public:

  BagFrameEmitterTest()
  {
    ros::Time::init();
    bag.open(BAG, rosbag::bagmode::Write);
    bag.write(INFO, ros::Time(1), sensor_msgs::CameraInfo());
    bag.write(COLOR, ros::Time(1), sensor_msgs::Image());
    bag.write(DEPTH, ros::Time(1), sensor_msgs::Image());
    bag.write(COLOR, ros::Time(3), sensor_msgs::Image());
    bag.write(DEPTH, ros::Time(3), sensor_msgs::Image());
    bag.write(COLOR, ros::Time(5), sensor_msgs::Image());
    bag.close();
    bag.open(BAG, rosbag::bagmode::Read);
  }

  ~BagFrameEmitterTest()
  {
    bag.close();
    std::remove(BAG);
  }

  rosbag::Bag bag;

};

TEST_F(BagFrameEmitterTest, Constructor)
{
  ASSERT_NO_THROW(BagFrameEmitter(bag, COLOR, DEPTH, INFO));
}

TEST_F(BagFrameEmitterTest, NonExistingCameraInfoTopic)
{
  ASSERT_THROW(BagFrameEmitter(bag, COLOR, DEPTH, "c"), std::runtime_error);
}

TEST_F(BagFrameEmitterTest, InvalidCameraInfoTopic)
{
  ASSERT_THROW(BagFrameEmitter(bag, COLOR, DEPTH, DEPTH), std::runtime_error);
}

TEST_F(BagFrameEmitterTest, GetCameraInfo)
{
  BagFrameEmitter bfe(bag, COLOR, DEPTH, INFO);
  ASSERT_TRUE(bfe.getCameraInfo());
}

TEST_F(BagFrameEmitterTest, Next)
{
  BagFrameEmitter bfe(bag, COLOR, DEPTH, INFO);
  ASSERT_TRUE((bool)bfe.next());
  ASSERT_TRUE((bool)bfe.next());
  ASSERT_FALSE((bool)bfe.next());
  ASSERT_FALSE((bool)bfe.next());
}

TEST_F(BagFrameEmitterTest, NextWithInvalidColorTopic)
{
  BagFrameEmitter bfe(bag, "/camera/rgb/image", DEPTH, INFO);
  ASSERT_FALSE((bool)bfe.next());
}

int main(int argc, char **argv)
{
  testing::InitGoogleTest(&argc, argv);
  return RUN_ALL_TESTS();
}

