#ifndef __KONZ_COMMON_ODOMETRY_H
#define __KONZ_COMMON_ODOMETRY_H

#include <memory>

#include "konz_common/aliases.h"
#include "konz_common/frame.h"
#include "konz_common/transform_filters/transform_filter.h"

namespace konz
{

namespace common
{

/** Base class for different odometry implementations. */
class Odometry
{

public:

  typedef std::unique_ptr<Odometry> UPtr;

  virtual ~Odometry() { }

  virtual void process(Frame::Ptr frame) = 0;

  virtual Transform getPose() const = 0;

  virtual Transform getDeltaTransform() const = 0;

  virtual bool keyframeCreatedAtLastCall() const = 0;

  virtual bool estimationFailedAtLastCall() const = 0;

  void setTransformFilter(transform_filters::TransformFilter::Ptr transform_filter)
  {
    transform_filter_ = transform_filter;
  }

protected:

  transform_filters::TransformFilter::Ptr transform_filter_;

};

}

}

#endif

