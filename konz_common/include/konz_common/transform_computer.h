#ifndef __KONZ_COMMON_TRANSFORM_COMPUTER_H
#define __KONZ_COMMON_TRANSFORM_COMPUTER_H

#include <memory>

#include "konz_common/aliases.h"
#include "konz_common/frame.h"

namespace konz
{

namespace common
{

/** Base class for different transform computation implementations. */
class TransformComputer
{

public:

  typedef std::unique_ptr<TransformComputer> UPtr;

  virtual ~TransformComputer() { }

  virtual bool compute(Frame::Ptr reference, Frame::Ptr target, Transform& transform) = 0;

  virtual bool compute(Frame::Ptr target, Transform& transform) = 0;

  virtual void setReferenceFrame(Frame::Ptr reference) = 0;

};

}

}

#endif

