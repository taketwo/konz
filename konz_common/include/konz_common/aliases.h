#ifndef __KONZ_COMMON_ALIASES_H
#define __KONZ_COMMON_ALIASES_H

#include <Eigen/Eigen>
#include <Eigen/Geometry>

namespace konz
{

namespace common
{

typedef float Scalar;

typedef Eigen::Matrix<Scalar, 3, 1> Vector3;
typedef Eigen::Matrix<Scalar, 3, 3> Matrix3;
typedef Eigen::Quaternion<Scalar> Quaternion;
typedef Eigen::Matrix<Scalar, Eigen::Dynamic, Eigen::Dynamic> Matrix;

typedef Eigen::Transform<Scalar, 3, Eigen::Affine> Transform;

}

}

#endif

