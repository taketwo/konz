#ifndef __KONZ_COMMON_UTIL_TRANSFORMS_FILE_H
#define __KONZ_COMMON_UTIL_TRANSFORMS_FILE_H

#include <string>
#include <iostream>
#include <fstream>

#include "konz_common/aliases.h"

namespace konz
{

namespace common
{

namespace util
{

class TransformsFile
{

public:

  TransformsFile(const std::string& filename);

  ~TransformsFile();

  void writeHeader(const std::string& title, const std::string& bag_filename);

  void writeLine(double timestamp1, double timestamp2, const Transform& transform, double runtime, bool succeeded);

private:

  std::ofstream file_stream_;

};

}

}

}

#endif

