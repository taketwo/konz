#ifndef __KONZ_COMMON_UTIL_INTERVAL_H
#define __KONZ_COMMON_UTIL_INTERVAL_H

#include <string>
#include <vector>

namespace konz
{

namespace common
{

namespace util
{

/** Utility structure that represents an interval with start and end points. */
struct Interval
{

  double start;
  double end;

  /** Check whether a value belongs to the interval (including boundary
    * points). */
  bool contains(double value) { return (start <= value && value <= end); }

};

typedef std::vector<Interval> Intervals;

/** Read an ASCII file into a vector of intervals.
  *
  * If the file does not exist then the returned vector will be empty. */
Intervals readIntervalsFile(const std::string& filename);

}

}

}

#endif

