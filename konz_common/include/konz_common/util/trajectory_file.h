#ifndef __KONZ_COMMON_UTIL_TRAJECTORY_FILE_H
#define __KONZ_COMMON_UTIL_TRAJECTORY_FILE_H

#include <string>
#include <iostream>
#include <fstream>

#include "konz_common/aliases.h"

namespace konz
{

namespace common
{

namespace util
{

class TrajectoryFile
{

public:

  TrajectoryFile(const std::string& filename);

  ~TrajectoryFile();

  void writeHeader(const std::string& title, const std::string& bag_filename);

  void writeLine(double timestamp, const Transform& transform, bool failed);

private:

  std::ofstream file_stream_;

};

}

}

}

#endif

