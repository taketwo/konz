#ifndef __KONZ_COMMON_UTIL_GEOMETRY_H
#define __KONZ_COMMON_UTIL_GEOMETRY_H

#include <algorithm>
#include <cmath>

namespace konz
{

namespace common
{

namespace util
{

template<typename T>
double transformAngle(T& transform)
{
  return acos(std::min(1.0, std::max(-1.0, (transform.rotation().trace() - 1.0) / 2.0)));
}

template<typename T>
double transformDistance(T& transform)
{
  return transform.translation().norm();
}

}

}

}

#endif

