#ifndef __KONZ_COMMON_UTIL_CONSOLE_FRAME_INDICATOR_H
#define __KONZ_COMMON_UTIL_CONSOLE_FRAME_INDICATOR_H

#include <boost/format.hpp>

namespace konz
{

namespace common
{

namespace util
{

class ConsoleFrameIndicator
{

public:

  ConsoleFrameIndicator(size_t frames_per_line = 100)
  : fpl_(frames_per_line)
  , frame_count_(0)
  {
    std::cout.precision(3);
  }

  void frame(bool succeeded, bool keyframe)
  {
    if (frame_count_++ % fpl_ == 0)
      std::cout << boost::format("\n[%4d] ") % frame_count_;
    if (!succeeded)
      std::cout << "\x1B[31m▪\e[m" << std::flush;
    else
      std::cout << (keyframe ? "◉" : "○") << std::flush;
  }

private:

  size_t fpl_;

  size_t frame_count_;

};

}

}

}

#endif

