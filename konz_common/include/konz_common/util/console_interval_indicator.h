#ifndef __KONZ_COMMON_UTIL_CONSOLE_INTERVAL_INDICATOR_H
#define __KONZ_COMMON_UTIL_CONSOLE_INTERVAL_INDICATOR_H

#include <boost/format.hpp>

#include "konz_common/util/interval.h"

namespace konz
{

namespace common
{

namespace util
{

class ConsoleIntervalIndicator
{

public:

  ConsoleIntervalIndicator(size_t interval_size)
  : first_ts_(-1)
  , interval_size_(interval_size)
  , interval_count_(0)
  {
    std::cout.precision(3);
  }

  void interval(Interval interval)
  {
    if (first_ts_ < 0)
      first_ts_ = interval.start;
    else
      std::cout << boost::format(" %.3f\n") % interval_.end;
    interval_ = Interval{interval.start - first_ts_, interval.end - first_ts_};
    std::cout << boost::format("[%04d/%04d] %.3f ") % ++interval_count_ % interval_size_ % interval_.start;
  }

  void transform(bool succeeded = true)
  {
    std::cout << (succeeded ? "◉" : "\x1B[31m▪\e[m") << std::flush;
  }

private:

  double first_ts_;

  size_t interval_size_;

  size_t interval_count_;

  Interval interval_;

};

}

}

}

#endif

