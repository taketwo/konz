#ifndef __KONZ_COMMON_EMITTERS_BAG_FRAME_EMITTER_H
#define __KONZ_COMMON_EMITTERS_BAG_FRAME_EMITTER_H

#include <string>
#include <vector>

#include <rosbag/bag.h>
#include <rosbag/view.h>
#include <sensor_msgs/Image.h>
#include <sensor_msgs/CameraInfo.h>
#include <message_filters/simple_filter.h>
#include <message_filters/synchronizer.h>
#include <message_filters/sync_policies/approximate_time.h>

#include "konz_common/emitters/frame_emitter.h"

namespace konz
{

namespace common
{

namespace emitters
{

class BagFrameEmitter : public FrameEmitter
{

public:

  typedef std::unique_ptr<BagFrameEmitter> UPtr;

  /** Construct a frame emitter for the given bag file and topic names.
    *
    * This emitter works under the assumption that the calibration of the camera
    * does not change over time. The very first CameraInfo message from the bag
    * file is stored and then is pushed into all emitted frames. */
  BagFrameEmitter(const rosbag::Bag& bag, const std::string& color_t, const std::string& depth_t, const std::string& camera_info_t);

  virtual Frame::Ptr next();

  sensor_msgs::CameraInfoPtr getCameraInfo() { return camera_info_msg_; }

private:

  void fetchCameraInfo(const std::string& info_t);

  template <class M>
  class BagSubscriber : public message_filters::SimpleFilter<M>
  { public: void newMessage(const boost::shared_ptr<M const> &msg) { signalMessage(msg); } };

  const rosbag::Bag& bag_;
  rosbag::View view_;
  rosbag::View::iterator view_iter_;
  const std::string color_topic_;
  const std::string depth_topic_;
  sensor_msgs::CameraInfoPtr camera_info_msg_;

  typedef message_filters::sync_policies::ApproximateTime<sensor_msgs::Image, sensor_msgs::Image> SyncPolicy;
  typedef message_filters::Synchronizer<SyncPolicy> Synchronizer;
  void synchronizerCallback(const sensor_msgs::ImageConstPtr& color_img, const sensor_msgs::ImageConstPtr& depth_img);
  BagSubscriber<sensor_msgs::Image> color_subscriber_;
  BagSubscriber<sensor_msgs::Image> depth_subscriber_;
  Synchronizer synchronizer_;
  static const int QUEUE_SIZE = 10;

  Frame::Ptr new_frame_;

};

}

}

}

#endif

