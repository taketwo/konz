#ifndef __KONZ_COMMON_EMITTERS_FRAME_EMITTER_H
#define __KONZ_COMMON_EMITTERS_FRAME_EMITTER_H

#include "konz_common/frame.h"

namespace konz
{

namespace common
{

namespace emitters
{

/** An abstract class which defines an interface for frame emission. */
class FrameEmitter
{

public:

  typedef std::unique_ptr<FrameEmitter> UPtr;

  /** Emit a new frame.
    *
    * @return nullptr if the source of frames is exhausted. */
  virtual Frame::Ptr next() = 0;

  virtual ~FrameEmitter() { }

};

}

}

}

#endif

