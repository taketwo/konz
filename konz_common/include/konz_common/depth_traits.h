#ifndef __KONZ_COMMON_CONVERTERS_DEPTH_TRAITS_H
#define __KONZ_COMMON_CONVERTERS_DEPTH_TRAITS_H

// The content of this file was taken from the file depth_traits.h in depth_image_proc package.
// Revision 459 - January 12th, 2012

#include <limits>

namespace konz
{

namespace common
{

// Encapsulate differences between processing float and uint16_t depths
template<typename T> struct DepthTraits {};

template<>
struct DepthTraits<uint16_t>
{
  static inline bool valid(uint16_t depth) { return depth != 0; }
  static inline float toMeters(uint16_t depth) { return depth * 0.001f; } // originally mm
};

template<>
struct DepthTraits<float>
{
  static inline bool valid(float depth) { return std::isfinite(depth); }
  static inline float toMeters(float depth) { return depth; }
};

}

}

#endif

