#ifndef __KONZ_COMMON_TRANSFORM_FILTERS_TRANSFORM_FILTER_H
#define __KONZ_COMMON_TRANSFORM_FILTERS_TRANSFORM_FILTER_H

#include <memory>

#include "konz_common/aliases.h"

namespace konz
{

namespace common
{

namespace transform_filters
{

class TransformFilter
{

public:

  typedef std::shared_ptr<TransformFilter> Ptr;

  virtual ~TransformFilter() { };

  virtual Transform apply(const Transform& transform) const = 0;

};

}

}

}

#endif /* __KONZ_COMMON_TRANSFORM_FILTERS_TRANSFORM_FILTER_H */

