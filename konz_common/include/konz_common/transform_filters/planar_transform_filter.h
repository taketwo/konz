#ifndef __KONZ_COMMON_TRANSFORM_FILTERS_PLANAR_TRANSFORM_FILTER_H
#define __KONZ_COMMON_TRANSFORM_FILTERS_PLANAR_TRANSFORM_FILTER_H

#include "konz_common/transform_filters/transform_filter.h"

namespace konz
{

namespace common
{

namespace transform_filters
{

class PlanarTransformFilter : public TransformFilter
{

public:

  PlanarTransformFilter(const Quaternion& camera_orientation)
  : camera_orientation_(camera_orientation)
  {
  }

  virtual Transform apply(const Transform& transform) const
  {
    Transform t = camera_orientation_ * transform * camera_orientation_.inverse();
    const float roll = 0;
    const float pitch = 0;
    float yaw = atan2f(t(1, 0), t(0, 0));
    Scalar A = cos(yaw);
    Scalar B = sin(yaw);
    Scalar C = cos(pitch);
    Scalar D = sin(pitch);
    Scalar E = cos(roll);
    Scalar F = sin(roll);
    Scalar DE = D * E;
    Scalar DF = D * F;
    t(0, 0) = A * C;  t(0, 1) = A * DF - B * E; t(0, 2) = B * F + A * DE;
    t(1, 0) = B * C;  t(1, 1) = A * E + B * DF; t(1, 2) = B * DE - A * F;
    t(2, 0) = -D;     t(2, 1) = C * F;          t(2, 2) = C * E;
    t(2, 3) = 0;
    return t;
  }

private:

  Quaternion camera_orientation_;

};

}

}

}

#endif /* __KONZ_COMMON_TRANSFORM_FILTERS_PLANAR_TRANSFORM_FILTER_H */

