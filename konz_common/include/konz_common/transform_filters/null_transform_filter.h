#ifndef __KONZ_COMMON_TRANSFORM_FILTERS_NULL_TRANSFORM_FILTER_H
#define __KONZ_COMMON_TRANSFORM_FILTERS_NULL_TRANSFORM_FILTER_H

#include "konz_common/transform_filters/transform_filter.h"

namespace konz
{

namespace common
{

namespace transform_filters
{

class NullTransformFilter : public TransformFilter
{

public:

  Transform apply(const Transform& transform) const { return transform; }

};

}

}

}

#endif /* __KONZ_COMMON_TRANSFORM_FILTERS_NULL_TRANSFORM_FILTER_H */

