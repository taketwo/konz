#ifndef __KONZ_COMMON_NODES_ODOMETRY_EVALUATION_H
#define __KONZ_COMMON_NODES_ODOMETRY_EVALUATION_H

#include <deque>
#include <string>
#include <vector>

#include <rosbag/bag.h>

#include "konz_common/emitters/bag_frame_emitter.h"
#include "konz_common/odometry.h"

namespace konz
{

namespace common
{

namespace nodes
{

class OdometryEvaluation
{

public:

  bool init(int argc, char* argv[]);

  void run();

protected:

  virtual void initOdometry() = 0;

  struct Arguments
  {
    std::string config;
    std::string output;
    int limit;
    bool planar;
    std::string camera_orientation;
    std::string bag;
    std::vector<std::string> topics;
    float qw, qx, qy, qz;
  } args_;

  emitters::BagFrameEmitter::UPtr emitter_;

  Odometry::UPtr odometry_;

  rosbag::Bag bag_;

};

}

}

}

#endif

