#ifndef __KONZ_COMMON_NODES_TRANSFORM_COMPUTER_EVALUATION_H
#define __KONZ_COMMON_NODES_TRANSFORM_COMPUTER_EVALUATION_H

#include <deque>
#include <string>
#include <vector>

#include <rosbag/bag.h>

#include "konz_common/emitters/bag_frame_emitter.h"
#include "konz_common/transform_computer.h"
#include "konz_common/util/interval.h"
#include "konz_common/util/console_interval_indicator.h"

namespace konz
{

namespace common
{

namespace nodes
{

class TransformComputerEvaluation
{

public:

  bool init(int argc, char* argv[]);

  void run();

protected:

  virtual void initTransformComputer() = 0;

  struct Arguments
  {
    std::string config;
    std::string intervals;
    std::string output;
    int limit;
    std::string bag;
    std::vector<std::string> topics;
  } args_;

  emitters::BagFrameEmitter::UPtr emitter_;

  TransformComputer::UPtr transform_computer_;

  rosbag::Bag bag_;

private:

  struct FrameQueue
  {
    FrameQueue(TransformComputerEvaluation& parent);
    Frame::Ptr pop(util::Interval interval);
    bool hasMore();
    Frame::Ptr next();
    TransformComputerEvaluation& parent_;
    std::deque<Frame::Ptr> deque_;
    util::Interval interval_;
    size_t index_;
  };

};

}

}

}

#endif

