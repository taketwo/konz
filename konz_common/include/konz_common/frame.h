#ifndef __KONZ_COMMON_FRAME_H
#define __KONZ_COMMON_FRAME_H

#include <memory>

#include <ros/assert.h>
#include <sensor_msgs/CameraInfo.h>
#include <sensor_msgs/Image.h>

namespace konz
{

namespace common
{

/** This is a convenience class that holds together pointers to the parts of an
  * RGBD frame, namely color and depth images, and camera info. */
class Frame
{

public:

  typedef std::shared_ptr<Frame> Ptr;
  typedef std::unique_ptr<Frame> UPtr;

  Frame(const sensor_msgs::ImageConstPtr& color_msg, const sensor_msgs::ImageConstPtr& depth_msg, const sensor_msgs::CameraInfoConstPtr& camera_info_msg)
  : color_msg_(color_msg)
  , depth_msg_(depth_msg)
  , camera_info_msg_(camera_info_msg)
  {
    ROS_ASSERT(color_msg);
    ROS_ASSERT(depth_msg);
    ROS_ASSERT(camera_info_msg);
    timestamp_ = color_msg_->header.stamp.toSec();
  }

  inline const sensor_msgs::ImageConstPtr& getColorImage() const { return color_msg_; }
  inline const sensor_msgs::ImageConstPtr& getDepthImage() const { return depth_msg_; }
  inline const sensor_msgs::CameraInfoConstPtr& getCameraInfo() const { return camera_info_msg_; }
  inline double getTimestamp() const { return timestamp_; }

private:

  sensor_msgs::ImageConstPtr color_msg_;
  sensor_msgs::ImageConstPtr depth_msg_;
  sensor_msgs::CameraInfoConstPtr camera_info_msg_;
  double timestamp_;

  /** Disable copy constructor. */
  Frame(const Frame& f) = delete;

  /** Disable assignment operator. */
  Frame& operator=(const Frame&) = delete;

};

}

}

#endif

